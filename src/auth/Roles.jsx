import React, { useEffect, useState } from 'react';
import { ApiRoles, https, startAndlimit } from '../Service/ConFigURL';
import { Table } from 'antd';
import ReactApexChart from 'react-apexcharts';

export default function Roles() {
    const [Roles, setRoles] = useState([]);

    useEffect(() => {
        const fetchRoles = async () => {
            try {
                const token = localStorage.getItem('token');
                if (token) {
                    const response = await https.post(ApiRoles, startAndlimit);
                    const rolesName = response.data.data.list;
                    setRoles(rolesName);
                }
            } catch (error) {
                console.log('error: ', error);
            }
        };

        fetchRoles();
    }, []);

    const chartOptions = {
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        },
    };

    const chartSeries = [
        {
            name: 'Monthly Data',
            data: [12, 19, 3, 5, 2],
        },
    ];


    const columns = [
        {
            title: 'Create At',
            dataIndex: 'code',
        },
        {
            title: 'Create by',
            dataIndex: 'createdBy',
        },
        {
            title: 'update by',
            dataIndex: 'createdAt',
        },
        {
            title: 'update at',
            dataIndex: 'updatedAt',
        },
        {
            title: 'Create At',
            dataIndex: 'code',
        },
        {
            title: 'Create by',
            dataIndex: 'createdBy',
        },
        {
            title: 'update by',
            dataIndex: 'createdAt',
        },
        {
            title: 'update at',
            dataIndex: 'updatedAt',
        },
    ];

    return (


        <div>

            <ReactApexChart
                options={chartOptions}
                series={chartSeries}
                type="line"
                height={350}
            />


            <div style={{ overflowX: 'auto' }}>
                <Table
                    dataSource={Roles}
                    columns={columns}
                    pagination={false}
                    scroll={{ x: 'max-content' }} // Đây là phần quan trọng để tạo thanh cuộn ngang
                />
            </div>
        </div>
    );
}

import React, { useEffect, useState } from 'react';
import { https, profilesUser, startAndlimit } from '../Service/ConFigURL';
import { Table } from 'antd';

export default function Auth() {
    const [nameUser, setNameUser] = useState([]);
    console.log('nameUser: ', nameUser);

    const profileUser = async () => {
        try {
            const token = localStorage.getItem('token');
            const response = await https.post(profilesUser, startAndlimit);

            if (token) {
                const userName = response.data.data.list;
                setNameUser(userName);
            }
        } catch (error) {
            console.log('error: ', error);
        }
    };

    useEffect(() => {
        profileUser();
    }, []);

    const filteredNameUser = nameUser.filter(user => user.fullName && user.fullName.trim() !== '');

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
        },
        {
            title: 'Active',
            dataIndex: 'active',
            render: active => active ? 'true' : 'false',
        },
        {
            title: 'Full Name',
            dataIndex: 'fullName',
            key: 'fullName',
        },
        {
            title: 'User name',
            dataIndex: 'login',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',

        },
        {
            title: 'Email',
            dataIndex: 'email',

        },
        {
            title: 'Create At',
            dataIndex: 'createdAt',

        },
        {
            title: 'Create by',
            dataIndex: 'createdBy',

        },
        {
            title: 'update by',
            dataIndex: 'updatedBy',

        },
        {
            title: 'update at',
            dataIndex: 'updatedAt',

        },

    ];

    return (
        <div>
            <Table dataSource={filteredNameUser} columns={columns} pagination={false} />
        </div>
    );
}

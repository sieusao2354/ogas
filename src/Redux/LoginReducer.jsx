import { LOGIN_SUCCESS } from "./LoginAction";

const initialState = {

}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:

            return {
                ...state,
                ...action.payload
            };


        default:
            return state
    }
}

export default userReducer
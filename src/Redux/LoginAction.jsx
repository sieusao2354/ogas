export const LOGIN_SUCCESS = "LOGIN_SUCCESS"

export const LoginSuccess = (useData) => {
    return {
        type: LOGIN_SUCCESS,
        payload: useData
    }
}


import React, { useEffect, useState } from 'react';
import { ApiQuanTrac, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import ReactApexChart from 'react-apexcharts';
import { DatePicker, Select, Table } from 'antd';
import dayjs from 'dayjs';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';

export default function QuanTracDemo() {
    const dateFormat = 'YYYY-MM-DD';
    const [VungNuoi, setVungNuoi] = useState([])
    const [selectAoId, setSelectAoId] = useState()

    const [parameter, setParameter] = useState([]);
    const [nameValue, setNameValue] = useState([])
    const [filteredData, setFilteredData] = useState([]);

    const [selectedThongsoid, setSelectedThongsoid] = useState(1)
    const [aoId, setAoId] = useState([])
    const [selectPond, setSelectPond] = useState([])
    const [startDate, setStartDate] = useState(dayjs().format(dateFormat)); // Đặt ngày bắt đầu là ngày hiện tại
    const [endDate, setEndDate] = useState(dayjs().format(dateFormat)); // Đặt ngày kết thúc là ngày hiện tại
    const [selectedPondId, setSelectedPondId] = useState(null);

    const [Value, setValue] = useState([])

    const { Option } = Select;


    const vungNuoi = async () => {
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(listFarmingArea, startAndlimit)
            if (token) {
                setVungNuoi(res.data.data.list)
                if (res.data.data.list && res.data.data.list.length > 0) {
                    setSelectAoId(res.data.data.list[0].id);
                }
            }

        } catch (err) {
            console.log('err: ', err);
        }
    }

    const pondId = async () => {
        try {
            const token = localStorage.getItem('token')
            const res = await https.post(loadFishPond, loadFishPondData)
            if (token) {
                setAoId(res.data.data.list)
                if (aoId.length > 0) {
                    setSelectPond(aoId[0].id);
                }
            }
        } catch (err) {
            console.log('err: ', err);
        }
    }
    useEffect(() => {
        vungNuoi()
        pondId()
    }, [])
    useEffect(() => {
        if (selectAoId) {
            const filteredPonds = aoId.filter(
                (item) =>
                    item.farmingArea && item.farmingArea.id === selectAoId
            );
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(aoId);
        }
    }, [selectAoId, aoId]);
    const changeValueCallApiNumber3 = (value) => {
        setSelectPond(value)
    };
    const handleSelectChange = (value) => {
        setSelectAoId(value);
    };


    useEffect(() => {
        fetchMonitoring()


    }, [selectedThongsoid, selectPond, endDate, startDate,])

    const handleStartDateChange = (date, dateString) => {

        setStartDate(dateString);

    };

    const handleEndDateChange = (date, dateString) => {

        setEndDate(dateString);

    };

    const fetchMonitoring = async () => {
        if (!selectPond || !selectedThongsoid || !startDate || !endDate) return
        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "aoid": selectPond,
                "thongsoid": selectedThongsoid
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiQuanTrac, params);

            if (token) {
                setParameter(res.data.data.datachart.lstDate);
                setNameValue(res.data.data.datatable[0].parameter)

                setValue(res.data.data.datatable)
            }

        } catch (error) {
            console.log('error: ', error);
        }
    }

    const handleThongsoidChange = (value) => {

        setSelectedThongsoid(parseInt(value));

    }

    const options = {

        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },

        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },

        noData: {
            text: "Không có dữ liệu",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },

        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    dataLabels: {
                        enabled: false,
                        position: "top",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold",
                            colors: [""],
                        },
                    },
                },
            },
        },

        xaxis: {
            categories: parameter.map((data) => {
                const dateParts = data.date.slice(5, 16)
                return dateParts
            }),
        },

        legend: {
            position: 'top',
        },

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [4, 0, 4]
        },

        colors: ['#EF4444', '#0E9CFF', '#EF4444'],

        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",

            },
        },

        yaxis: {
            show: true,
            min: selectedThongsoid === 1 ? 0 : selectedThongsoid === 2 ? -2 : selectedThongsoid === 3 ? 20 : undefined,
            max: selectedThongsoid === 1 ? 10 : selectedThongsoid === 2 ? 14 : selectedThongsoid === 3 ? 40 : undefined,
        },

        series: [
            {
                name: 'UCL',
                data: parameter.map((item) => item.ucl),
                strokeWidth: 1,
            },
            {
                name: nameValue.name,
                data: parameter.map((item) => item.value),
                strokeWidth: 1,
            },
            {
                name: 'LCL',
                data: parameter.map((item) => item.lcl),
                strokeWidth: 1,
            }
        ],

    };

    useEffect(() => {
        fetchMonitoring();
    }, [selectedThongsoid]);

    const columns = [
        {
            title: "Vùng nuôi",

            dataIndex: "fishPond",
            key: "fishPond",
            render: (fishPond) => <span>{fishPond.farmingArea.name}</span>
        },
        {
            title: "Tên ao",

            dataIndex: "fishPond",
            key: "fishPond",
            render: (fishPond) => <span>{fishPond.name}</span>
        },
        {
            title: "Ngày đo",
            dataIndex: "timeStamp",
            key: "timeStamp",
            render: (timeStamp) => <span>{timeStamp.slice(0, 11)}</span>
        },
        {
            title: "Giờ đo",
            dataIndex: "createdAt",
            key: "createdAt",
            render: (createdAt) => <span>{createdAt.slice(11, 16)}</span>
        },
        {
            title: "UCL",
            dataIndex: "ucl",
            key: "ucl",
        },
        {
            title: nameValue.name,
            dataIndex: "value",
            key: "value",
        },
        {
            title: "LCL",
            dataIndex: "lcl",
            key: "lcl",

        },


        {
            title: "Thông báo",
            dataIndex: "evaluate",
            key: "evaluate",
            render: (text) => {
                if (text === "FAIL") {
                    return <p className='bg-red-500 text-white w-24 h-6 text-center' style={{ borderRadius: 3 }} >Không Đạt</p>
                } else if (text === "PASS") {
                    return <p className='bg-green-500 text-center w-24 h-6 text-white' style={{ borderRadius: 3 }} > Đạt</p>
                }
            }
        },
    ];

    return (
        // <div className='responsive_select '>
        //     <div className=' ' style={{ fontWeight: 400, fontSize: 14, marginTop: 6, width: '100%' }}>
        //         <div className='flex' style={{ marginLeft: 3 }} >
        //             <AiFillHome size={20} />
        //             <a className='px-2' href="">Home {' /'} </a>
        //             <p> Quan Trắc  </p>
        //         </div>
        //         <hr />

        //         <div className='flex'>
        //             <Select
        //                 bordered={false}
        //                 value={selectAoId}
        //                 onChange={handleSelectChange}
        //                 defaultValue={"Chọn vùng nuôi"}
        //             >
        //                 {VungNuoi.map((item) => (
        //                     <Select key={item.id} value={item.id}>
        //                         <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
        //                             {item.name}
        //                         </div>
        //                     </Select>
        //                 ))}
        //             </Select>
        //             <Select
        //                 bordered={false}
        //                 defaultValue="Chọn ao"
        //                 onChange={changeValueCallApiNumber3}>

        //                 {filteredData.map((item) => (
        //                     <Select.Option key={item.id} value={item.id}>
        //                         <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
        //                             {item.name}
        //                         </div>
        //                     </Select.Option>
        //                 ))}
        //             </Select>

        //             <Select style={{ width: 150 }} value={selectedThongsoid} onChange={handleThongsoidChange}>
        //                 <Option value={1}>pH</Option>
        //                 <Option value={2}>DO</Option>
        //                 <Option value={3}>Nhiệt độ</Option>
        //             </Select>

        //             <DatePicker
        //                 onChange={handleStartDateChange}
        //                 defaultValue={dayjs()} // Đặt giá trị mặc định là ngày hiện tại
        //                 format={dateFormat} // Định dạng ngày cho DatePicker

        //             />

        //             <DatePicker
        //                 onChange={handleEndDateChange}
        //                 defaultValue={dayjs()} // Đặt giá trị mặc định là ngày hiện tại
        //                 format={dateFormat} // Định dạng ngày cho DatePicker

        //             />
        //         </div>
        //     </div>

        //     <div className="  py-3 "  >
        //         <div className='flex ' >
        //             <div style={{ width: '1%' }} ></div>

        //             <div className='bg-white ' style={{ borderRadius: 5, width: '98%' }}>
        //                 <div className="text-lg text-titleBorder " style={{ color: " white", fontWeight: 400, background: "#036E9B", }} >

        //                     <p className=' px-2' >Biểu đồ tiêu thụ thức ăn theo ngày

        //                     </p>

        //                 </div>
        //             </div>
        //             <div style={{ width: '1%' }} ></div>
        //         </div>
        //         <div className='flex' >
        //             <div style={{ width: '1%' }}  >
        //             </div>
        //             <div className='bg-white' style={{ width: '98%', borderRadius: 5 }} >
        //                 <ReactApexChart
        //                     options={options}
        //                     series={options.series}
        //                     type="line"
        //                     height={450}
        //                 />
        //             </div>
        //             <div style={{ width: '1%' }}  >
        //             </div>
        //         </div>
        //         <div className='flex py-3' >
        //             <div style={{ width: '1%' }}  >
        //             </div>
        //             <div style={{ width: '98%' }}  >
        //                 <Table dataSource={Value} pagination={{ pageSize: 10 }} columns={columns} />
        //             </div>
        //             <div style={{ width: '1%' }}  >
        //             </div>
        //         </div>
        //     </div>

        // </div>
        <div className='nuni-font  responsive_select  '   >
            <div className=' bg-white ' style={{ paddingLeft: 10, }}>
                <div>
                    <div className=' flex bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                        <div className=' flex ' style={{ fontWeight: 400, fontSize: 15, alignItems: 'center', }}>
                            <AiOutlineHome size={20} />
                            <a className='px-2' >Home {' /'} </a>


                            <Select
                                bordered={false}
                                value={selectAoId}
                                onChange={handleSelectChange}
                                defaultValue={"Chọn vùng nuôi"}
                            >
                                {VungNuoi.map((item) => (
                                    <Select key={item.id} value={item.id}>
                                        <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                            {item.name}
                                        </div>
                                    </Select>
                                ))}
                            </Select>
                            -
                            <Select
                                bordered={false}
                                defaultValue="Chọn ao"
                                onChange={changeValueCallApiNumber3}>

                                {filteredData.map((item) => (
                                    <Select.Option key={item.id} value={item.id}>
                                        <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                            {item.name}
                                        </div>
                                    </Select.Option>
                                ))}
                            </Select>
                            <Select style={{ width: 150 }} value={selectedThongsoid} onChange={handleThongsoidChange}>
                                <Option value={1}>pH</Option>
                                <Option value={2}>DO</Option>
                                <Option value={3}>Nhiệt độ</Option>
                            </Select>
                        </div>
                        <div className='date-picker-container ' >

                            <DatePicker
                                onChange={handleStartDateChange}
                                defaultValue={dayjs()} // Đặt giá trị mặc định là ngày hiện tại
                                format={dateFormat} // Định dạng ngày cho DatePicker

                            />

                            <DatePicker
                                onChange={handleEndDateChange}
                                defaultValue={dayjs()} // Đặt giá trị mặc định là ngày hiện tại
                                format={dateFormat} // Định dạng ngày cho DatePicker

                            />

                        </div>
                    </div>
                    <hr />
                </div>
            </div>

            <div className="  py-3 "  >
                <div className='flex ' >
                    <div style={{ width: 10 }} ></div>

                    <div className='bg-white ' style={{ borderRadius: 5, width: '100%' }}>
                        <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>Biểu đồ hiển thị thông số quan trắc</p>
                        </div>
                    </div>
                    <div style={{ width: 10 }} ></div>
                </div>
                <div className='flex' >
                    <div style={{ width: 10 }}  >
                    </div>
                    <div className='bg-white' style={{ width: '100%', borderRadius: 5 }} >
                        <ReactApexChart
                            options={options}
                            series={options.series}
                            type="line"
                            height={450}

                        />
                    </div>
                    <div style={{ width: 10 }}  >
                    </div>
                </div>
                <div className='py-3' >
                    <div className='flex ' >
                        <div style={{ width: 10 }} ></div>

                        <div className='bg-white ' style={{ borderRadius: 5, width: "100%" }}>
                            <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                                <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>Lịch sử đo thông số quan trắc
                                </p>
                            </div>
                        </div>
                        <div style={{ width: 10 }} ></div>
                    </div>
                    <div className='flex' >

                        <div style={{ width: 10 }}  >
                        </div>
                        <div style={{ width: '100%' }}  >
                            <Table dataSource={Value} pagination={{ pageSize: 10 }} columns={columns} />
                        </div>
                        <div style={{ width: 10 }}  >
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}


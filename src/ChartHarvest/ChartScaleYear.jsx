import React, { useEffect, useState } from 'react'
import { ApiScaleYear, ContentType, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import moment from 'moment';
import { DatePicker, Select } from 'antd';
import { customYear } from '../chart/DatePickerViVn';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';
import ReactApexChart from 'react-apexcharts';

export default function ChartScaleYear() {

    const [loadNameFaming, setLoadNameFarming] = useState([]);// load vùng nuôi
    const [pondData, setPondData] = useState([]) // load ao nuôi
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();// dùng để liên kết ao và chart 
    const [filteredData, setFilteredData] = useState([]);// hàm để hứng

    const [selectedYearScale, setSelectedYearScale] = useState(moment().year());
    const [loadScaleYear, setLoadScaleYear] = useState([]);
    const [selectIdScaleYear, setSelectIdScaleYear] = useState()

    const [selectedPondId, setSelectedPondId] = useState(null);

    useEffect(() => {//  API load tên vùng nuôi
        const fetchData = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(listFarmingArea, startAndlimit)
                if (token) {
                    setLoadNameFarming(response.data.data.list);
                }
                if (response.data.data.list && response.data.data.list.length > 0) {
                    setSelectedFarmingArea(response.data.data.list[0].id);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        fetchData();
    }, []);


    useEffect(() => {// API load tên Ao
        const loadPond = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(loadFishPond, loadFishPondData, {
                    headers: {
                        "Content-Type": ContentType,
                    },
                });
                if (token) {
                    setPondData(response.data.data.list);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        loadPond();
    }, []);

    useEffect(() => {// API tìm tên Ao trùng id với chart
        if (selectedFarmingAreaId) {
            const filteredPonds = pondData.filter((item) => item.farmingArea && item.farmingArea.id === selectedFarmingAreaId);
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(pondData);
        }
    }, [selectedFarmingAreaId, pondData]);


    useEffect(() => {
        // Lấy id của phần tử đầu tiên trong danh sách ao sau khi filteredData được cập nhật
        if (filteredData && filteredData.length > 0) {
            setSelectedPondId(filteredData[0].id);
            loadChartScaleYear(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu
        }
    }, [filteredData]);
    const changeValueCallApiNumber3 = (value) => {// hàm để gọi id cho Vùng Nuôi và ao để liên kết với chart có ao nuôi
        setSelectIdScaleYear(Array.isArray(value) ? value[0] : value)

    };

    const loadChartScaleYear = async () => {
        if (!selectedYearScale || !selectIdScaleYear) return;
        const Params = {
            data: {
                year: selectedYearScale,
                aoid: selectIdScaleYear,
            },
        };

        try {
            const token = localStorage.getItem("token");
            const response = await https.post(ApiScaleYear, Params);
            if (token) {
                const data = response.data.data.datachart?.[0];
                if (data) {
                    const newSeries = { data: data.lstBigDecimal };
                    setLoadScaleYear([newSeries]);

                }
            }
        } catch (err) {
            console.log("err: ", err);
        }
    };

    const optionsYearScale = {

        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },

        labels: ["Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th 9", "Th 10", "Th 11", "Th 12",],

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#FF5252"],
                fontWeight: 400,

            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 2,
                borderWidth: 1,
                borderColor: '#fff',
                opacity: 0.9,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#000',
                    opacity: 0.45
                }
            },
        },

    }

    const handleYearChange = (date, dateString) => {
        if (date) {
            setSelectedYearScale(date.year());
        }
    };
    useEffect(() => {
        loadChartScaleYear();
    }, [selectedYearScale, selectIdScaleYear]);



    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value)
    }


    return (
        <div className='nuni-font' style={{ height: '100vh', }}>

            <div className='flex bg-white ' style={{ paddingLeft: 10, }}>

                <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                    <div className=' flex ' style={{ fontWeight: 400, fontSize: 15, alignItems: 'center', }}>

                        <AiOutlineHome size={20} />
                        <a className='px-2' >Home  </a>
                        <p>/</p>
                        <Select
                            bordered={false}
                            value={selectedFarmingAreaId}
                            onChange={handleSelectChange}
                            defaultValue={"Chọn vùng nuôi"}
                        >
                            {loadNameFaming.map((item) => (
                                <Select key={item.id} value={item.id}>
                                    <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                        {item.name}
                                    </div>

                                </Select>
                            ))}
                        </Select>
                        <Select
                            bordered={false}
                            defaultValue="Chọn Ao"
                            onChange={changeValueCallApiNumber3}
                            value={selectedPondId}

                        >
                            {filteredData.map((item) => (
                                <Select.Option key={item.id} value={item.id}>
                                    <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                        {item.name}
                                    </div>
                                </Select.Option>
                            ))}
                        </Select>
                    </div>


                </div>
                <hr />
            </div>

            <div className='flex py-3   ' >
                <div style={{ width: '1%' }} ></div>
                <div style={{ width: '98%' }} >
                    <div className='bg-white ' style={{ borderRadius: 5 }}>

                        <div className="text-xs text-titleBorder flex" style={{ color: "white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >Biểu đồ tiêu thụ thức ăn theo ngày (Kg)
                            </p>


                        </div>
                        <div className='date-picker-container' >

                            <DatePicker

                                onChange={handleYearChange}
                                picker="year"
                                locale={customYear}
                                allowClear={true}
                            />
                        </div>
                        <ReactApexChart
                            options={optionsYearScale}
                            series={loadScaleYear}
                            type="bar"
                            height={400}
                        />

                    </div>
                </div>
                <div style={{ width: '1%' }} ></div>
            </div>


        </div>
    )
}

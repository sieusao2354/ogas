import React, { useEffect, useState } from 'react'
import { ApiScaleByDay, ContentType, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import { Button, DatePicker, Select, Table, Tabs } from 'antd';
import { customLocale } from '../chart/DatePickerViVn';
import ReactApexChart from 'react-apexcharts';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';
import { useTranslation } from 'react-i18next';

export default function ChartScale() {

    const { t, i18n } = useTranslation()

    const [loadNameFaming, setLoadNameFarming] = useState([]);// load vùng nuôi
    const [pondData, setPondData] = useState([]) // load ao nuôi
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();// dùng để liên kết ao và chart 
    const [filteredData, setFilteredData] = useState([]);// hàm để hứng

    const [dataChartScale, setDataChartScale] = useState([])
    const [startDateScale, setStartDateScale] = useState(null);
    const [endDateScale, setEndDateScale] = useState(null);
    const [dataFishPondScale, setDataFishPondScale] = useState([]);
    const [datePickerValueScale, setDatePickerValueScale] = useState(null);
    const [nameTableScale, setNameTableScale] = useState(null);
    const [filteredTableDataScale, setFilteredTableDataScale] = useState([]);

    const [selectedPondId, setSelectedPondId] = useState(null);

    useEffect(() => {//  API load tên vùng nuôi
        const fetchData = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(listFarmingArea, startAndlimit)
                if (token) {
                    setLoadNameFarming(response.data.data.list);
                }
                if (response.data.data.list && response.data.data.list.length > 0) {
                    setSelectedFarmingArea(response.data.data.list[0].id);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {// API load tên Ao
        const loadPond = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(loadFishPond, loadFishPondData, {
                    headers: {
                        "Content-Type": ContentType,
                    },
                });
                if (token) {
                    setPondData(response.data.data.list);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        loadPond();
    }, []);

    const changeValueCallApiNumber3 = (value) => {// hàm để gọi id cho Vùng Nuôi và ao để liên kết với chart có ao nuôi
        setSelectedPondId(value);


        const id = Array.isArray(value) ? value[0] : value
        loadChartScale(id)
    };

    useEffect(() => {
        // Lấy id của phần tử đầu tiên trong danh sách ao sau khi filteredData được cập nhật
        if (filteredData && filteredData.length > 0) {
            setSelectedPondId(filteredData[0].id);
            loadChartScale(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu

        }
    }, [filteredData]);


    useEffect(() => {// API tìm tên Ao trùng id với chart
        if (selectedFarmingAreaId) {
            const filteredPonds = pondData.filter((item) => item.farmingArea && item.farmingArea.id === selectedFarmingAreaId);
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(pondData);
        }
    }, [selectedFarmingAreaId, pondData]);

    const currentDates = new Date()//biến lấy ngày tháng trên hệ thống
    const endDateChartPond = currentDates.toISOString("en-US")// tạo ra biến gán vào chart để lấy ngày mới nhất trên hệ thống 

    const loadChartScale = async (id) => {
        const Params = {
            "data": {
                "startdate": "2023-01-01",
                "enddate": endDateChartPond,
                "aoid": id
            }
        }
        try {
            const token = localStorage.getItem('token')
            const res = await https.post(ApiScaleByDay, Params)

            if (token) {

                setDataChartScale(res.data.data.datachart[0].lstDate)
                setDataFishPondScale(res.data.data.datachart[0].fishPond);

                const processedData = res.data.data.datatable.map((item) => ({
                    id: item.id,
                    feeding: item.createdAt.slice(11, 16),
                    quantity: item.netMass,
                    lotnum: item.lotNum,
                    time: item.timeScan.slice(0, 10),
                    soAo: item.fishPond.name,
                    STT: item.ordinalNum,
                    farmingArea: item.fishPond.farmingArea.name,
                }));
                setNameTableScale(processedData);

                const defaultEndDateScale = new Date();
                const defaultStartDateScale = new Date(
                    defaultEndDateScale.getTime() - 15 * 24 * 60 * 60 * 1000
                );
                setStartDateScale(defaultStartDateScale);
                setEndDateScale(defaultEndDateScale);
                setDatePickerValueScale([defaultStartDateScale, defaultEndDateScale]);
            }
        } catch (err) {
            console.log('err: ', err);

        }
    }

    let filterDataScale = dataChartScale;
    if (startDateScale && endDateScale) {
        filterDataScale = dataChartScale.filter((item) => {
            const date = new Date(item.date);
            return date >= startDateScale && date <= endDateScale;
        });
    }


    const handleDateChangeScale = (dates) => {
        if (dates && dates.length >= 1) {
            const newEndDateScale = new Date(dates[1]);
            const start = new Date(dates[0]);
            const end = new Date(newEndDateScale.getUTCFullYear(), newEndDateScale.getUTCMonth(), newEndDateScale.getUTCDate());

            start.setHours(0, 0, 0, 0); // Set time to midnight (00:00:00)
            end.setHours(0, 0, 0, 0); // Set time to midnight (00:00:00)

            setStartDateScale(start);
            setEndDateScale(end);
            setDatePickerValueScale(dates);


            const filteredData = nameTableScale.filter((item) => {
                const date = new Date(item.time);
                date.setHours(0, 0, 0, 0);
                return date >= start && date <= end;
            });
            setFilteredTableDataScale(filteredData);
        }
        else {
            setEndDateScale(new Date());
            setDatePickerValueScale(null);
            setFilteredTableDataScale(nameTableScale);
        }
    };

    const optionsKgScaleBar = { // option chỉnh sửa của loadChart( load thức ăn theo kg, ngày và tháng)
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },
        xaxis: {
            categories: filterDataScale.map((data) => {
                const dateParts = data.date.slice(5, 10).split("-");
                const reveredDates = dateParts.reverse().join("/");
                return reveredDates;
            }),
        },

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#FF5252"],
                fontWeight: 400,

            }
        },


    };

    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value)
    }
    const handleDelete = async (id) => {
        try {
            const confirmed = window.confirm("Bạn có chắn chắn muốn xóa? ")
            if (confirmed) {
                await https.delete(`/harvest/${id}`);
                setNameTableScale((prevTable) => prevTable.filter((item) => item.id !== id));

            }

        } catch (error) {
            console.error(error);
        }
    };

    const columnScale = [
        {
            title: <p className='nuni-font ' >{t('scaleTable.areaName')}</p>,

            dataIndex: "farmingArea",
            render: (farmingArea) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{farmingArea}</p>,

        },
        {
            title: <p className='nuni-font ' >{t('scaleTable.pondName')}</p>,

            dataIndex: "soAo",
            render: (fishPond) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{fishPond}</p>,

        },
        {
            title: <p className='nuni-font '>{t('scaleTable.quantity')}</p>,

            dataIndex: "quantity",
            key: "quantity",
            render: (quantity) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{quantity} Kg</p>,

        },

        {
            title: <p className='nuni-font '>{t('scaleTable.dayHarvest')}</p>,

            dataIndex: "time",
            render: (time) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{time.slice(0, 11)}</p>,

        },
        {
            title: <p className='nuni-font '>{t('scaleTable.timeHarvest')}</p>,

            dataIndex: "feeding",
            render: (feeding) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{feeding}</p>,

        },
        {
            title: <p className='nuni-font '>{t('scaleTable.action')}</p>,

            key: 'action',
            render: (text, record) => (
                <button style={{ background: '#FF5252', borderRadius: 5 }} onClick={() => handleDelete(record.id)}><p style={{ fontWeight: 400, padding: 5, color: 'white' }} className='text-table-responsiveValue' >Delete</p></button>
            ),
        }


    ];


    //  ----------------------------------------------------------------------------------------------------


    return (
        <div style={{ height: '100vh' }} className='nuni-font' >
            <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                <div className=' flex ' style={{ fontWeight: 400, fontSize: 14, alignItems: 'center', }}>
                    <AiOutlineHome size={20} />
                    <a className='px-2' >Home </a>
                    <p>/</p>
                    <Select
                        bordered={false}
                        value={selectedFarmingAreaId}
                        onChange={handleSelectChange}
                        defaultValue={"Chọn vùng nuôi"}
                    >
                        {loadNameFaming.map((item) => (
                            <Select key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                    {item.name}
                                </div>

                            </Select>
                        ))}
                    </Select>

                    <Select
                        bordered={false}
                        defaultValue="Chọn Ao"
                        onChange={changeValueCallApiNumber3}
                        value={selectedPondId}

                    >
                        {filteredData.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                    {item.name}
                                </div>
                            </Select.Option>
                        ))}
                    </Select>
                </div>
                <div>
                    <DatePicker.RangePicker
                        // style={{ position: 'relative', right: 0, zIndex: 9999, height: 32 }}

                        onChange={handleDateChangeScale}
                        onOk={(dates) => {
                            setStartDateScale(dates[0]);
                            setEndDateScale(dates[1]);
                            setDatePickerValueScale(dates);
                        }}
                        locale={customLocale}

                    />
                </div>


            </div>
            <div className=' flex py-3  '>


                <div style={{ width: '1%' }} ></div>
                <div style={{ width: '98%' }}>
                    <div className='bg-white' style={{ borderRadius: 5, }}>
                        <div className="text-xs text-titleBorder flex " style={{ color: "white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('scale.title')}
                            </p>
                        </div>

                        <div className="" >
                            <ReactApexChart
                                options={optionsKgScaleBar}
                                series={[{ data: filterDataScale.map((item) => item.netMass), strokeWidth: 1 }]}
                                type="bar"
                                height={400}


                            />
                        </div>
                    </div>
                    <div className='py-5' >
                        <div className="text-xs text-titleBorder flex " style={{ color: "white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('scale.titleTable')}
                            </p>                        </div>
                        <Table className='' dataSource={nameTableScale} style={{}} columns={columnScale} />
                    </div>
                </div>
                <div style={{ width: '1%' }} ></div>

            </div>


        </div>
    )
}

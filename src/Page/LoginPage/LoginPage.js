import { Button, Form, Input, message } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./LoginPage.css";
import { BaseUrlLogin, ContentType } from "../../Service/ConFigURL";
import { useState } from "react";

import logoHS from "./img/logoDPES1.svg"

import logoEsyFeed from "./img/Logo-05.svg"
import { useDispatch } from "react-redux";
import { LoginSuccess } from "../../Redux/LoginAction";


const LoginPage = () => {


    const dispatch = useDispatch()

    const history = useNavigate();
    const [isLoading, setIsLoading] = useState(false);


    const onFinish = async (values) => {
        setIsLoading(true);

        try {
            const { username, password } = values;
            const res = await axios.post(
                BaseUrlLogin,
                { username, password },
                {
                    headers: {
                        "Content-Type": ContentType,
                    },
                }
            );
            const token = res.data.data.token;
            const nameUser = res.data.data.session.fullName;

            const session = res.data.data;
            localStorage.setItem('fullName', nameUser)
            localStorage.setItem("token", token);

            if (token) {
                history("/Dashboard");
            }
            dispatch(LoginSuccess(session))

        } catch (err) {
            console.log("err: ", err);
            message.error(
                "Đăng nhập không thành công. Vui lòng kiểm tra tên đăng nhập và mật khẩu."
            );
        } finally {
            setIsLoading(false);
        }
    };



    return (

        <div className=" login-container" >

            <div className="flex imglogologin" style={{ width: '50%', }} >
                <div className="flex" style={{ position: 'absolute', top: 50, left: 50 }}>
                    <img style={{ width: 58.5, height: 30, }} src={logoHS} alt="" /><p style={{ color: '#ED1D26', fontWeight: 800, lineHeight: 'normal', marginTop: 8 }}>CONTROL SDN BHD</p>
                </div>
                <div className="" style={{ position: 'absolute', bottom: 50, left: 50, fontSize: 11, fontWeight: 400 }}>
                    <p className="text-gray-300" >© 2022-2023 Aucontech Co., Ltd. All rights reserved.</p>
                    <div className="flex text-gray-300">
                        <p>Privacy Policy</p>
                        <p>Terms of Service</p>
                    </div>
                </div>
            </div>

            <div className="login-form-container " >
                <div className="flex text-lg"> <p>Welcome to</p> <p className="px-2" style={{ color: '#779341' }}> DPES</p></div>
                <div className="login-form-logo-container py-2">
                    <img className="fishsh" src={logoEsyFeed} alt="" />
                </div>
                <div className="login-form">
                    <p className="sign-in" >Sign in</p>
                    <Form name="normal_login" className="login-form py-3" onFinish={onFinish}>
                        <p className="flex none-text-login ">Enter your username</p>

                        <Form.Item
                            name="username"
                            rules={[{ required: true, message: "Please enter username" }]}
                            className="py-2"
                        >
                            <Input
                                className="login-pass-id"
                                placeholder="Username or email address"
                            />
                        </Form.Item>
                        <div className="flex none-text-login ">
                            <p className="flex  ">Enter your password</p>
                            <p className="flex" style={{ justifyContent: 'flex-end', marginLeft: 'auto', cursor: 'pointer', color: '#0E9CFF' }} > Forgot your password?</p>
                        </div>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
                            className="py-2"

                        >

                            <Input.Password
                                className="login-pass-id"
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                loading={isLoading}
                            >
                                Sign in
                            </Button>
                        </Form.Item>
                    </Form>

                </div>
            </div>

        </div >
    );
};

export default LoginPage;

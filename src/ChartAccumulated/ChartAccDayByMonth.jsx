import React, { useEffect, useState } from 'react'
import moment from 'moment';
import dayjs from 'dayjs';
import { ContentType, GetSumMassByYear, getSumByDay, getSumByMonth, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import { DatePicker, Select } from 'antd';
import ReactApexChart from 'react-apexcharts';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';
import { customLocale, customMonth, customYear } from '../chart/DatePickerViVn';
import "./MenuCss.css"
import { useTranslation } from 'react-i18next';
const yearFormat = 'YYYY';
const MonthFormat = 'MM';


export default function ChartAccDayByMonth() {

    const { t, i18n } = useTranslation()


    const [loadNameFaming, setLoadNameFarming] = useState([]);// load vùng nuôi
    const [pondData, setPondData] = useState([]) // load ao nuôi
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();// dùng để liên kết ao và chart 
    const [filteredData, setFilteredData] = useState([]);// hàm để hứng , lấy trực tiếp ao nuôi được lấy từ state pondData

    const [seriesMonthByYear, setSeriesMonthByYear] = useState([]);
    const [selectedMonth, setSelectedMonth] = useState(moment().month() + 1);
    const [selectedMonthByYear, setSelectedMonthByYear] = useState(moment().year());
    const [aoIDMonthByYear, setAoIDMonthByYear] = useState('');

    const [series, setSeries] = useState([]);
    const [selectedYear, setSelectedYear] = useState(moment().year());
    const [aoIdYear, setAoIdYear] = useState('')

    const [pondDataY, setPondDataY] = useState([]);
    const [filteredDataY, setFilteredDataY] = useState([]);
    const [selectedFarmingAreaName, setSelectedFarmingAreaName] = useState("");
    const [selectedYearY, setSelectedYearY] = useState(moment().year());

    const [dataFishPondKg, setDataFishPondKg] = useState([]);

    const [selectedPondId, setSelectedPondId] = useState(null);

    const [nameFishPond, setNameFishPond] = useState([])

    useEffect(() => {//  API load tên vùng nuôi
        const fetchData = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(listFarmingArea, startAndlimit)
                if (token) {
                    setLoadNameFarming(response.data.data.list);
                }
                if (response.data.data.list && response.data.data.list.length > 0) {
                    setSelectedFarmingArea(response.data.data.list[0].id);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {// API load tên Ao
        const loadPond = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(loadFishPond, loadFishPondData, {
                    headers: {
                        "Content-Type": ContentType,
                    },
                });
                if (token) {
                    setPondData(response.data.data.list);
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        loadPond();
    }, []);

    useEffect(() => {// API tìm tên Ao trùng id với chart
        if (selectedFarmingAreaId) {
            const filteredPonds = pondData.filter((item) => item.farmingArea && item.farmingArea.id === selectedFarmingAreaId);
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(pondData);
        }
    }, [selectedFarmingAreaId, pondData]);

    const changeValueCallApiNumber3 = (value) => {// hàm để gọi id cho Vùng Nuôi và ao để liên kết với chart có ao nuôi
        setSelectedPondId(value);

        setAoIDMonthByYear(Array.isArray(value) ? value[0] : value);
        setAoIdYear(Array.isArray(value) ? value[0] : value)

    };
    useEffect(() => {
        // Lấy id của phần tử đầu tiên trong danh sách ao sau khi filteredData được cập nhật
        if (filteredData && filteredData.length > 0) {
            setSelectedPondId(filteredData[0].id);
            setAoIDMonthByYear(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu
            setAoIdYear(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu

        }
    }, [filteredData]);

    const handleGetSumMonthByYear = async () => {
        if (!selectedMonth || !selectedMonthByYear || !aoIDMonthByYear) return;
        const Params = {
            data: {
                month: selectedMonth,
                year: selectedMonthByYear,
                aoid: aoIDMonthByYear,
            },
        };
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(getSumByDay, Params);
            setDataFishPondKg(res.data.data.lstdatachart[0].fishPond)

            if (token) {
                const data = res.data.data.lstdatachart?.[0];
                if (data) {
                    const newSeries = { data: data.lstLong.map(value => value / 1000) };
                    setSeriesMonthByYear([newSeries]);
                }
            }
        } catch (err) {
            console.log('err: ', err);
        }
    };
    const handleGetSumByMonth = async () => {
        if (!selectedYear || !aoIdYear) return;
        const Params = {
            data: {
                year: selectedYear,
                aoid: aoIdYear
            }
        }
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(getSumByMonth, Params);
            if (token) {
                const data = res.data.data.lstdatachart?.[0];

                if (data) {
                    const nameFishPond = { name: data.fishPond.name }
                    setNameFishPond([nameFishPond])
                    const newSeries = { data: data.lstLong.map(value => value / 1000) };
                    setSeries([newSeries]);
                }
            }
        } catch (err) {
            // Xử lý lỗi nếu cần
        }
    }

    useEffect(() => {
        handleGetSumByMonth()
    }, [selectedYear, aoIdYear])

    const handleDatePickerChange = (date, dateString) => {
        if (date) {
            const year = date.year();
            setSelectedYear(year.toString());
        } else {
            setSelectedYear('');
        }
    }


    const handleDateChangeMonthByYear = date => {
        setSelectedMonth(date.month() + 1);
        setSelectedMonthByYear(date.year());
    };

    useEffect(() => {
        handleGetSumMonthByYear();
    }, [aoIDMonthByYear, selectedMonth, selectedMonthByYear]);

    const serie = [
        10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310,
    ];

    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value)
        const selectedArea = loadNameFaming.find((item) => item.id === value);
        if (selectedArea) {
            setSelectedFarmingAreaName(selectedArea.name);
        }
    }


    const optionsYearBar = {

        chart: {
            type: 'area',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 0.5, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {
                columnWidth: '70%',
                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },

        labels: ["Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th 9", "Th 10", "Th 11", "Th 12",],

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 0.1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: true,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 400,

            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 2,
                borderWidth: 1,
                borderColor: '#fff',
                opacity: 0.9,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#000',
                    opacity: 0.45
                }
            },
        },

    }


    const optionsMonthByYearBar = { // option chỉnh sửa của loadChart( load thức ăn theo kg, ngày và tháng)
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },

        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 0.5, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },

        labels: serie.map((value, index) => ` ${index + 1}`),

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 0.1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: false,
            showForSingleSeries: false,
            showForNullSeries: false,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 400,

            },

        },


    };


    const chartData = filteredDataY.map((item) => item.lstLong[0] / 1000);

    const chartnameData = filteredDataY.map((item) => item.fishPond.name)
    const options = {
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 0.5, // Độ mờ của các dòng lưới
            },
        },
        plotOptions: {
            bar: {
                columnWidth: '70%',

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },
        labels: chartnameData,

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 400,

            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 2,
                borderWidth: 1,
                borderColor: '#fff',
                opacity: 0.9,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#000',
                    opacity: 0.45
                }
            },
        },
    }

    useEffect(() => {
        if (selectedFarmingAreaId) {
            const filteredPonds = pondDataY.filter(
                (item) =>
                    item.fishPond && item.fishPond.farmingArea.id === selectedFarmingAreaId
            );
            setFilteredDataY(filteredPonds);
        } else {
            setFilteredDataY(pondDataY);
        }
    }, [selectedFarmingAreaId, pondDataY]);



    const handleDatePickerYear = date => {
        setSelectedYearY(date.year())

    }

    useEffect(() => {
        fetchPond()
    }, [selectedYearY, loadNameFaming])

    const fetchPond = async () => {
        if (!selectedYearY || !loadNameFaming) return;
        const Params = {
            data: {
                year: selectedYearY
            }
        }
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(GetSumMassByYear, Params);
            if (token) {
                setPondDataY(response.data.data.lstdatachart);
                setDataFishPondKg(response.data.data.datachart[0].fishPond);

            }
        } catch (err) {
            // console.log("err: ", err);
        }
    };



    const renderSelectPage = () => {
        return <div className=' flex bg-white' style={{ paddingLeft: 10, zIndex: 999 }}>
            <div className=' flex' style={{ fontWeight: 400, fontSize: 14, }}>
                <div className=' flex ' style={{ fontWeight: 400, fontSize: 14, alignItems: 'center', }}>
                    <AiOutlineHome size={20} />
                    <a className='px-2' >Home  </a>
                    <p>/</p>
                    <Select
                        bordered={false}
                        value={selectedFarmingAreaId}
                        onChange={handleSelectChange}
                        defaultValue={"Chọn vùng nuôi"}
                    >
                        {loadNameFaming.map((item) => (
                            <Select key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#', textAlign: 'center' }}>
                                    {item.name}
                                </div>

                            </Select>
                        ))}
                    </Select>
                    <p >/</p>

                    <Select
                        bordered={false}
                        defaultValue="Chọn Ao"
                        onChange={changeValueCallApiNumber3}
                        value={selectedPondId}

                    >
                        {filteredData.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                    {item.name}
                                </div>
                            </Select.Option>
                        ))}
                    </Select>


                </div>
            </div>



        </div>
    }

    const renderPc = () => {
        return <div className='' >
            <div className='flex container-chart  ' style={{ padding: 10, gap: 5 }} >
                <div className='chartKg bg-white' >

                    <div className=" text-titleBorder flex " style={{ color: " white", background: "#036E9B", }} >

                        <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >
                            {t('Accumulated.titleMonthByYear')}{selectedYear} {t('mass.tons')} </p>

                    </div>
                    <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto', padding: 5 }} >

                        <DatePicker
                            onChange={handleDatePickerChange}
                            picker="year"
                            allowClear={true}
                            defaultValue={dayjs(moment().format("yyyy"), yearFormat)} format={yearFormat}
                        />
                    </div>

                    <div className='bg-white borderChartBottom ' >
                        <ReactApexChart
                            options={optionsYearBar}
                            series={series}

                            type="bar"
                            height={300}
                        />
                    </div>
                </div>


                <div className='chartKg bg-white' >

                    <div className=" text-titleBorder flex " style={{ color: " white", background: "#036E9B" }} >

                        <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >
                            {t('Accumulated.titleAllPondByYear')}{selectedYearY} {t('mass.tons')} </p>

                    </div>
                    <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto', padding: 5 }} >

                        <DatePicker
                            defaultValue={dayjs(moment().format("yyyy"), yearFormat)}
                            format={yearFormat}
                            picker='year'
                            onChange={handleDatePickerYear} />
                    </div>
                    <div className='bg-white borderChartBottom '>
                        <ReactApexChart

                            options={options}
                            series={[{ name: dataFishPondKg.name, data: chartData }]}
                            type="bar"
                            height={300}
                        />
                    </div>
                </div>

            </div>
            <div className='flex ' >
                <div style={{ width: 10 }} ></div>
                <div style={{ width: '100%' }} >
                    <div className='bg-white ' style={{ borderRadius: 5 }}>

                        <div className=" text-titleBorder " style={{ color: " white", background: "#036E9B", }} >

                            <p style={{ padding: 7, fontWeight: 500, display: 'flex', marginLeft: 5, fontSize: 15 }} >
                                {t('Accumulated.titleDayByMonthOfYear')} {selectedMonth}  {t('year.titleYear')} {selectedMonthByYear} {t('mass.tons')}    </p>

                        </div>
                        <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto', padding: 5 }} >

                            <DatePicker

                                onChange={handleDateChangeMonthByYear}
                                picker="month"
                                allowClear={true}
                                format='MM-YYYY'
                                defaultValue={dayjs(moment().format("MM"), MonthFormat)}

                            />
                        </div>
                        <ReactApexChart

                            options={optionsMonthByYearBar}
                            series={seriesMonthByYear}
                            type="bar"
                            height={300} />

                    </div>
                </div>
                <div style={{ width: 10 }} ></div>
            </div>
        </div >
    }


    return (

        <div className='nuni-font' style={{ height: '100vh' }}>
            {renderSelectPage()}
            {renderPc()}



        </div >
    )
}

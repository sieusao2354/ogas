import React, { useEffect, useState } from 'react'
import { ApiAccumulatedFishPond, ContentType, https, listFarmingArea, startAndlimit } from '../Service/ConFigURL';
import { DatePicker, Select, Table, Tabs } from 'antd';
import ReactApexChart from 'react-apexcharts';
import { customLocale } from '../chart/DatePickerViVn';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';
import TabPane from 'antd/es/tabs/TabPane';
import { useTranslation } from 'react-i18next';

export default function ChartAccArea() {
    const { t, i18n } = useTranslation()

    const [loadNameFaming, setLoadNameFarming] = useState([]);
    const [pondData, setPondData] = useState([]);
    const [selectedFarmingAreaId, setSelectedFarmingAreaId] = useState();
    const [filteredData, setFilteredData] = useState([]);

    const [pondDataTons, setPondDataTons] = useState([]);
    const [nameTable, setNameTable] = useState([])
    const [selectStartDate, setSelectStartDate] = useState();
    const [selectEndDate, setSelectEndDate] = useState();
    const [filteredDataTons, setFilteredDataTons] = useState([]);

    useEffect(
        () => {
            const fetchData = async () => {
                try {
                    const token = localStorage.getItem("token");
                    const response = await https.post(listFarmingArea, startAndlimit, {
                        headers: {
                            "Content-Type": ContentType,
                        },
                    });
                    if (token) {
                        setLoadNameFarming(response.data.data.list);
                    }
                    if (response.data.data.list && response.data.data.list.length > 0) {
                        setSelectedFarmingAreaId(response.data.data.list[0].id);
                    }
                } catch (err) {
                    console.log("err: ", err);
                }
            };
            fetchData();
        }, []
    );

    useEffect(() => {
        fetchPond()
    }, [selectEndDate, selectStartDate, loadNameFaming])

    const Dates = new Date()//biến lấy ngày tháng trên hệ thống
    const nowDate = Dates.toISOString("en-US")// tạo ra biến gán vào chart để lấy ngày mới nhất trên hệ thống 

    const fetchPond = async () => {
        if (!selectEndDate || !selectStartDate || !loadNameFaming) return;
        const params = {
            data: {
                startdate: selectStartDate ? selectStartDate : "2023-01-01",
                enddate: selectEndDate ? selectEndDate : nowDate
            }
        };
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(ApiAccumulatedFishPond, params);
            if (token) {
                setPondDataTons(response.data.data.datachart);

                setPondData(response.data.data.datachart);
                const processedData = response.data.data.datatable.map((item) => ({
                    feeding: item.createdAt.slice(11, 16),
                    quantity: item.quantity,
                    lotnum: item.lotNum,
                    time: item.timeScan.slice(0, 10),
                    soAo: item.fishPond.name,
                    STT: item.ordinalNum,
                    farmingArea: item.fishPond.farmingArea.name
                }))
                setNameTable(processedData)

            }
        } catch (err) {
            console.log("err: ", err);
        }
    };

    useEffect(() => {
        if (selectedFarmingAreaId) {
            const filteredPonds = pondDataTons.filter(
                (item) =>
                    item.fishPond && item.fishPond.farmingArea.id === selectedFarmingAreaId
            );
            setFilteredDataTons(filteredPonds);
        } else {
            setFilteredDataTons(pondDataTons);
        }
    }, [selectedFarmingAreaId, pondData]);

    const handleSelectChange = (value) => {
        setSelectedFarmingAreaId(value);
    }

    { t('AccumulatedTable.pondName') }
    const columns = [
        {
            title: <p className='text-table-responsive' >{t('AccumulatedTable.areaName')}</p>,
            dataIndex: "farmingArea",
            render: (farmingArea) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{farmingArea}</p>,
        },
        {
            title: <p className='text-table-responsive' >{t('AccumulatedTable.pondName')}</p>,
            dataIndex: "soAo",
            render: (fishPond) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{fishPond}</p>,
        },
        {
            title: <p className='text-table-responsive'>    {t('AccumulatedTable.quantity')}
            </p>,
            dataIndex: "quantity",
            key: "quantity",
            render: (quantity) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{quantity} Kg</p>,
        },
        {
            title: <p className='text-table-responsive'>    {t('AccumulatedTable.dayExport')}
            </p>,
            dataIndex: "time",
            render: (time) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{time.slice(0, 11)}</p>,
        },
        {
            title: <p className='text-table-responsive'>{t('AccumulatedTable.timeExport')}</p>,
            dataIndex: "feeding",
            render: (feeding) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{feeding.slice(0, 11)}</p>,
        },
        {
            title: <p className='text-table-responsive' >    {t('AccumulatedTable.ordinalNum')}
            </p>,
            dataIndex: "STT",
            render: (STT) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{STT} </p>,
        },
        {
            title: <p className='text-table-responsive' >{t('AccumulatedTable.lotNum')}
            </p>,
            dataIndex: "lotnum",
            render: (lotnum) => <p style={{ fontWeight: 400 }} className='text-table-responsiveValue' >{lotnum} </p>,
        },
    ];

    const chartLabels = filteredDataTons.map((item) => item.mapCountFeed["450"])
    const chartLabels500 = filteredDataTons.map((item) => item.mapCountFeed["500"])

    const chartnameData = filteredDataTons.map((item) => item.fishPond.name)
    const chartData = filteredDataTons.map((item) => item.sumMass / 1000)

    const optionsTons = {
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 0.5, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },
        labels: chartnameData,


        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B'],

        tooltip: {
            enabled: true,
            theme: "dark",

        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '13px',
                colors: ["#000"],
                fontWeight: 400,

            },

        },
    }

    const options = {

        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 0.5, // Độ mờ của các dòng lưới
            },
        },

        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },
        labels: chartnameData,


        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B', "#429D44"],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },

        legend: {
            show: true,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 800,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '13px',
                colors: ["#000"],
                fontWeight: 400,

            },
            background: {
                enabled: false,
                foreColor: "#fff",
                padding: 4,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: "#fff",
                opacity: 1,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    opacity: 1,
                },
            },
        },
    }

    const renderSelectPage = () => {
        return <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', zIndex: 1 }}>
            <div className=' flex ' style={{ fontWeight: 400, fontSize: 14, alignItems: 'center', }}>

                <AiOutlineHome size={20} />
                <a className='px-2' >Home  </a>
                <p>/</p>
                <Select
                    bordered={false}
                    value={selectedFarmingAreaId}
                    onChange={handleSelectChange}
                    defaultValue={"Chọn vùng nuôi"}

                >
                    {loadNameFaming.map((item) => (
                        <Select key={item.id} value={item.id}>
                            <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#', textAlign: 'center' }}>
                                {item.name}
                            </div>

                        </Select>
                    ))}
                </Select>
            </div>

            <div className='datePicker' >

                <DatePicker.RangePicker
                    value={[selectStartDate, selectEndDate]}
                    onChange={(dates) => {
                        setSelectStartDate(dates[0]);
                        setSelectEndDate(dates[1]);
                    }}
                    locale={customLocale}
                    format="DD-MM-YYYY"
                    getPopupContainer={(trigger) => trigger.parentNode}

                />
            </div>
        </div>
    }
    const resPc = () => {
        return (
            <div className='' >
                <div className='flex container-chart  ' style={{ padding: 10, gap: 5 }}  >
                    <div className='chartKg' >
                        <div className="text-xs text-titleBorder flex " style={{ color: " white", background: "#036E9B", }} >

                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}  >{t('Accumulated.titleByTimeKg')}
                            </p>
                        </div>
                        <div className='bg-white' style={{ borderRadius: 5, }}>
                            <ReactApexChart
                                options={optionsTons}
                                series={[{ data: chartData }]}
                                type="bar"
                                height={350}
                            />
                        </div>
                    </div>


                    <div className='chartKg' >

                        <div className="text-xs text-titleBorder flex " style={{ color: " white", background: "#036E9B", }} >

                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('Accumulated.titleByTimeBag')}

                            </p>

                        </div>
                        <div className='bg-white' style={{ borderRadius: 5, }}>
                            <ReactApexChart
                                options={options}
                                series={[{ name: "450Kg", data: chartLabels }, { name: "500kg", data: chartLabels500 }]}
                                type="bar"
                                height={350}
                            />

                        </div>
                    </div>
                </div>
                <div className='flex ' >
                    <div style={{ width: 10 }} ></div>

                    <div className='' style={{ width: '100%' }}  >
                        <div className="text-xs text-titleBorder flex " style={{ color: "white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('Accumulated.titleHisFoodByTime')}</p>

                        </div>
                        <Table className="" dataSource={nameTable} columns={columns} />

                    </div>
                    <div style={{ width: 10 }} ></div>

                </div>
            </div>
        )
    }


    return (
        <div className='nuni-font' style={{ height: '100vh' }} >
            {renderSelectPage()}
            {resPc()}

        </div>
    )
}

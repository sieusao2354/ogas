import React, { useEffect, useState } from 'react';
import { Button, Drawer, Dropdown, Layout, Menu, Modal } from 'antd';
import { MdLanguage, MdOutlineToday } from 'react-icons/md';
import { FcOvertime } from 'react-icons/fc';
import { AiOutlineOrderedList } from 'react-icons/ai';
import { AiOutlineControl, AiTwotoneSetting } from 'react-icons/ai';
import { BiChevronLeftCircle, BiUserCircle } from 'react-icons/bi';
import { useTranslation } from 'react-i18next';


import { BsCalendar2Day, } from 'react-icons/bs';

import { PiChatDotsLight } from 'react-icons/pi';
import { BsFillGridFill } from 'react-icons/bs';

import { BiChevronRightCircle } from 'react-icons/bi';

import { LiaBarsSolid } from 'react-icons/lia';
import Aucontech from "./img/logoDPES.svg"
import { TbCashBanknote } from 'react-icons/tb';
import ChartAccDayByMonth from './ChartAccDayByMonth';
import ChartAccArea from './ChartAccArea';
import QuanTrac from '../chart/QuanTrac';
import SystemPump from '../DeviceControl/SystemPump';
import "./MenuCss.css"
import ChartKgAndBag from '../ChartReport/ChartKgAndBag';
import ChartScaleYear from '../ChartHarvest/ChartScaleYear';
import { BiLogOut } from 'react-icons/bi';

import { useSelector } from 'react-redux';
import flagVN from "./img/flagVietnamese.png"
import flagUK from "./img/flagUK.png"
import Testcode from './Testcode';
import TestPumps from './TestPumps';
import HomeDashBoard from './HomeDashBoard';
const { Sider, Content } = Layout;

const SubMenu = ({ title, icon, items, onClick }) => {
    return (
        <Menu.SubMenu key={title} icon={icon} title={title}>
            {items.map((item) =>
                item.children ? (
                    <SubMenu
                        key={item.key}
                        title={item.label}
                        icon={item.icon}
                        items={item.children}
                        onClick={onClick}
                    />
                ) : (
                    <Menu.Item key={item.key} icon={item.icon} onClick={onClick}>
                        {item.label}
                    </Menu.Item>
                )
            )}
        </Menu.SubMenu>
    );
};

const ChartMenuAcc = () => {


    const { t, i18n } = useTranslation();


    const fullName = useSelector(state => state.userReducer.fullName);
    const nameUser = localStorage.getItem('fullName')

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
        window.location.href = "/";
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const [selectedComponent, setSelectedComponent] = useState('component1');
    const [collapsed, setCollapsed] = useState(false); // State để lưu trạng thái của menu
    const [collapseMobile, setColapseMobile] = useState(false)

    const handleMenuClick = (e) => {
        setSelectedComponent(e.key);
        localStorage.setItem('selectedComponent', e.key);
    };




    const handleCollapse = () => {
        setCollapsed(!collapsed); // Thay đổi trạng thái collapsed khi nhấn nút
    };
    useEffect(() => {
        const storedComponent = localStorage.getItem('selectedComponent');
        if (storedComponent) {
            setSelectedComponent(storedComponent);
        }
        setColapseMobile(!collapseMobile)
    }, []);

    const renderComponent = () => {
        if (!selectedComponent) {
            setSelectedComponent('component1');
        }
        switch (selectedComponent) {
            case 'component9':
                return <HomeDashBoard />
            case 'Accumulated':
                return <ChartAccDayByMonth />;
            case 'component5':
                return <QuanTrac />;
            case 'component6':
                return <ChartAccArea />;
            case 'component7':
                return <ChartScaleYear />;
            case 'component8':
                return <ChartKgAndBag />;
            case 'component11':
                return <SystemPump />;
            case 'TestCode':
                return <Testcode />;

            case 'User':
                return <SystemPump />;
            case 'Roles':
                return <TestPumps />;

            case 'Addnewlistmobile':
                return <Testcode />;
            default:
                return null;
        }
    };
    const menuItems = [
        { icon: <BsFillGridFill size={17} />, key: 'component9', label: <span  >Over view</span> },
        { icon: <AiOutlineControl size={17} />, key: 'TestCode', label: <span  >Control History</span> },

    ]
    const menuItems2 = [
        { icon: <PiChatDotsLight />, key: 'Roles', label: <span className='nuni-font'  > Control Report</span>, },
        { icon: <AiOutlineOrderedList size={17} />, key: 'component6', label: <span className='nuni-font'  >Historical Data</span> },
        { icon: <BsCalendar2Day size={17} />, key: 'component7', label: <span className='nuni-font'  >Scheduled</span> },
        { icon: <TbCashBanknote size={17} />, key: 'component8', label: <span className='nuni-font'  >Income</span> },
    ];


    const handleMenuDrawerClick = (e) => {
        setSelectedComponent(e.key);
        localStorage.setItem('selectedComponent', e.key);
        setColapseMobile(true);
    };
    const toggleLanguage = () => {
        const newLanguage = i18n.language === 'en' ? 'vi' : 'en';
        i18n.changeLanguage(newLanguage);
    };

    const menuTranslation = (
        <Menu>
            <Menu.Item key="vi">
                <Button style={{ borderWidth: 0 }} className='flex' onClick={() => toggleLanguage('vi')}>

                    <img style={{ width: 30, height: 20 }} src={flagVN} alt="" />


                    <p className='px-2' >{t('translation.vi')}</p>
                </Button>
            </Menu.Item>
            <Menu.Item key="en">
                <Button style={{ borderWidth: 0 }} className='flex' onClick={() => toggleLanguage('en')}>
                    <img style={{ width: 30, height: 20 }} src={flagUK} alt="" /> <p className='px-2' >English</p>
                </Button>
            </Menu.Item>
        </Menu>
    );


    const dropdownMenuMobile = (
        <Menu key='1' >
            <Menu.Item key='1' >
                <p style={{ fontSize: 13, color: 'gray' }}>{t('user.user')}</p>

                <p className='user_name px-2' style={{ fontSize: 15, }} >
                    {nameUser}
                </p>
                <hr />

            </Menu.Item>
            <Menu.Item>
                <Dropdown overlay={menuTranslation} trigger={['click']}>
                    <Button style={{ borderWidth: 0, fontSize: 20, cursor: 'pointer', display: 'flex' }}>
                        {i18n.language === 'vi' ? <MdLanguage size={24} /> : <MdLanguage size={24} />} <p className='px-2' style={{ fontSize: 15 }}>{t('translation.vi')}</p>
                    </Button>
                </Dropdown>
            </Menu.Item>


            <Menu.Item key='2' >
                <div className='logout-button' style={{ display: 'flex', alignItems: 'center', marginRight: 20, }} onClick={showModal}>

                    <BiLogOut size={25} className='hover:text-gray-500' />
                    <p style={{ marginLeft: 20 }}>{t('logout.logout')}</p>
                </div>
            </Menu.Item>
        </Menu>
    )

    const menuUser = (
        <Menu>
            <Menu.Item key='1' >
                <p style={{ fontSize: 13, color: 'gray' }}>{t('user.user')}</p>

                <p className='user_name ' style={{ fontSize: 15, padding: 5 }} >
                    {nameUser}
                </p>
                <hr />
            </Menu.Item>
            <Menu.Item key='2' >
                <div className='logout-button' style={{ display: 'flex', alignItems: 'center', marginRight: 20, }} onClick={showModal}>

                    <BiLogOut size={25} className='hover:text-gray-500' />
                    <p style={{ marginLeft: 20 }}>{t('logout.logout')}</p>
                </div>
            </Menu.Item>
        </Menu>
    )


    return (
        <div className='nuni-font '  >
            <div className='bg-white nav_bar_pc ' style={{ fontFamily: 'Rubik, sans-serif', }} >

                <div className=' flex' style={{ width: '100%', background: '#036E9B', position: 'fixed', zIndex: 9 }}>
                    <div className='bg-white  flex' style={{ width: 200, height: 50 }}>
                        <img className='' style={{ width: 150, height: 35, marginLeft: 20, marginTop: 8 }} src={Aucontech} alt="" />
                    </div>
                    <button
                        onClick={() => setCollapsed(!collapsed)}
                        type="text"
                        style={{
                            fontSize: '35px',
                            borderRadius: 50,
                            position: 'absolute',
                            left: 182,
                            top: 8,
                            color: 'white',
                            background: 'gray'
                        }}
                    >
                        {collapsed ? < BiChevronRightCircle /> : <BiChevronLeftCircle />}
                    </button>


                    <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto', alignItems: 'center', marginRight: 20, zIndex: 10 }}>
                        <Dropdown overlay={menuTranslation} trigger={['click']}>
                            <Button style={{ borderWidth: 0, fontSize: 20, cursor: 'pointer' }}>
                                {i18n.language === 'en' ? <MdLanguage size={24} style={{ color: '#fff' }} /> : <MdLanguage size={24} style={{ color: '#fff' }} />}
                            </Button>
                        </Dropdown>
                        <Dropdown overlay={menuUser} trigger={['click']}>
                            <BiUserCircle size={25} style={{ color: 'white', cursor: 'pointer' }} />
                        </Dropdown>

                    </div>

                    <Modal
                        title="Xác nhận"
                        visible={isModalVisible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <p>{t('logout.status')}</p>
                    </Modal>
                </div>

                <div>
                    <Layout clas style={{ paddingTop: 52, }}  >
                        <Sider className='nuni-font' style={{
                            overflowY: 'auto',
                            height: '100%',
                            position: 'fixed',
                            left: 0,
                            background: '#fff',
                            top: 50,
                            transition: 1,
                            boxShadow: "4px 4px 4px 0px rgba(0, 0, 0, 0.25)",

                        }}
                            collapsed={collapsed} onCollapse={handleCollapse}>
                            <div  >
                                <div  >
                                    <hr />
                                    <br />

                                    <p style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >{t('menu.control')}</p>
                                    <Menu style={{ textAlign: 'left', fontSize: 14, letterSpacing: 0.3, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuClick}>
                                        {menuItems.map((item) =>
                                            item.children ? (
                                                <SubMenu

                                                    key={item.key}
                                                    title={item.label}
                                                    icon={item.icon}
                                                    items={item.children}
                                                    onClick={handleMenuClick}
                                                />
                                            ) : (
                                                <Menu.Item
                                                    key={item.key}
                                                    icon={item.icon}
                                                    style={{ textAlign: 'flex' }}
                                                    onClick={handleMenuClick}>

                                                    {item.label}
                                                </Menu.Item>
                                            )
                                        )}
                                    </Menu>

                                </div>
                                <div>
                                    <hr />
                                    <br />
                                    <p className='' style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >Report Statistic  </p>

                                    <Menu className='' style={{ textAlign: 'left', fontSize: 14, letterSpacing: 0.3, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuClick}>

                                        {menuItems2.map((item) =>
                                            item.children ? (
                                                <SubMenu

                                                    key={item.key}
                                                    title={item.label}
                                                    icon={item.icon}
                                                    items={item.children}
                                                    onClick={handleMenuClick}
                                                />
                                            ) : (
                                                <Menu.Item key={item.key} icon={item.icon} style={{ textAlign: 'flex' }} onClick={handleMenuClick}>
                                                    {item.label}
                                                </Menu.Item>
                                            )
                                        )}
                                    </Menu>

                                </div>

                                <br />
                            </div>
                        </Sider>
                    </Layout >
                    <Layout style={{ marginLeft: collapsed ? 80 : 200, }} >
                        <Content style={{}} >{renderComponent()}</Content>
                    </Layout>
                </div>
            </div >
            <div className='bg-white nav_bar_mobile ' style={{ fontFamily: 'Rubik, sans-serif', }} >

                <div className=' flex' style={{ width: '100%', background: '#036E9B', position: 'fixed', zIndex: 9, height: 45, }}>
                    <div className='logout-button text-white' style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto', alignItems: 'center', marginRight: 20, zIndex: 9 }} >


                        <Dropdown overlay={dropdownMenuMobile} trigger={['click']}  >
                            <BiUserCircle size={25} style={{ color: 'white', cursor: 'pointer', }} />

                        </Dropdown>

                    </div>
                    <div className=''>
                        <button
                            onClick={() => setColapseMobile(!collapseMobile)}
                            type="text"
                            style={{

                                fontSize: '30px',
                                borderRadius: 50,
                                position: 'absolute',
                                left: 15,
                                top: 7,
                                color: 'white',
                            }}
                        >
                            {collapseMobile ? <LiaBarsSolid /> : ''}
                        </button>
                    </div>
                    <Modal
                        style={{ width: 200 }}
                        title={t('logout.confirm')}
                        visible={isModalVisible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <p>{t('logout.status')}</p>
                    </Modal>
                </div>
                <div>

                    <Layout style={{ paddingTop: 52, }}  >
                        <Drawer
                            placement="left"
                            closable={false}
                            onClose={() => setColapseMobile(true)}
                            visible={!collapseMobile}
                            width={200}
                            style={{ padding: 0, width: 300 }}
                        >
                            <div  >
                                <div className='bg-white  flex' style={{ width: 200, height: 50 }}>
                                    <img className='' style={{ width: 150, height: 35, marginLeft: 50, marginTop: 5 }} src={Aucontech} alt="" />                                </div>
                                <hr />
                                <br />
                                <p className='' style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >{t('menu.control')}</p>

                                <Menu style={{ textAlign: 'left', fontSize: 11, fontWeight: 300, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuDrawerClick}>

                                    {menuItems.map((item) =>
                                        item.children ? (
                                            <SubMenu
                                                key={item.key}
                                                title={item.label}
                                                icon={item.icon}

                                                items={item.children}
                                                onClick={handleMenuDrawerClick}
                                            />
                                        ) : (
                                            <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuDrawerClick}>
                                                {item.label}
                                            </Menu.Item>
                                        )
                                    )}
                                </Menu>
                            </div>
                            <div  >
                                <hr />
                                <br />
                                <p className='' style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >{t('menu.report')} </p>

                                <Menu style={{ textAlign: 'left', fontSize: 11, fontWeight: 300, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuDrawerClick}>

                                    {menuItems2.map((item) =>
                                        item.children ? (
                                            <SubMenu
                                                key={item.key}
                                                title={item.label}
                                                icon={item.icon}

                                                items={item.children}
                                                onClick={handleMenuDrawerClick}
                                            />
                                        ) : (
                                            <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuDrawerClick}>
                                                {item.label}
                                            </Menu.Item>
                                        )
                                    )}
                                </Menu>
                            </div>
                            {/* <div  >
                                <hr />
                                <br />
                                <p className='' style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >Quan trắc </p>

                                <Menu style={{ textAlign: 'left', fontSize: 11, fontWeight: 300, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuDrawerClick}>

                                    {menuItems3.map((item) =>
                                        item.children ? (
                                            <SubMenu
                                                key={item.key}
                                                title={item.label}
                                                icon={item.icon}

                                                items={item.children}
                                                onClick={handleMenuDrawerClick}
                                            />
                                        ) : (
                                            <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuDrawerClick}>
                                                {item.label}
                                            </Menu.Item>
                                        )
                                    )}
                                </Menu>
                            </div>
                            <div  >
                                <hr />
                                <br />
                                <p className='' style={{ display: collapsed ? 'none' : 'block', textAlign: 'left', justifyContent: 'left', fontSize: 15, marginLeft: 20 }} >Thu hoạch </p>

                                <Menu style={{ textAlign: 'left', fontSize: 11, fontWeight: 300, }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuDrawerClick}>

                                    {menuItems4.map((item) =>
                                        item.children ? (
                                            <SubMenu
                                                key={item.key}
                                                title={item.label}
                                                icon={item.icon}

                                                items={item.children}
                                                onClick={handleMenuDrawerClick}
                                            />
                                        ) : (
                                            <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuDrawerClick}>
                                                {item.label}
                                            </Menu.Item>
                                        )
                                    )}
                                </Menu>
                            </div> */}
                            <hr />
                        </Drawer>

                        <Layout>
                            <Content style={{}} >{renderComponent()}</Content>
                        </Layout>

                    </Layout >
                </div>
            </div >
        </div >
    );
};

export default ChartMenuAcc;



import { DatePicker, Modal, Select, Switch } from 'antd'
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { AiFillHome } from 'react-icons/ai'
import { ApiOnOffControl, https } from '../Service/ConFigURL';
import * as XLSX from 'xlsx';
import ReactApexChart from 'react-apexcharts';
import { SiMicrosoftexcel } from 'react-icons/si';

export default function HomeDashBoard() {
    const dateFormat = 'YYYY-MM-DD';
    const { t, i18n } = useTranslation()

    const [loadStation, setLoadStation] = useState([]);
    const [loadStation1, setLoadStation1] = useState([]);


    const [startDate, setStartDate] = useState(dayjs().format(dateFormat));
    const [endDate, setEndDate] = useState(dayjs().format(dateFormat));
    const [parameter, setParameter] = useState([]);
    const [nameValue, setNameValue] = useState([])
    const [lastValue, setLastValue] = useState(0);

    const [parameter1, setParameter1] = useState([]);
    const [nameValue1, setNameValue1] = useState([])
    const [lastValue1, setLastValue1] = useState(0);
    const [TimeStamp, setTimeStamp] = useState(0)

    const FetchStations = async () => {
        const params = {
            "data": {
                "deviceid": 12
            }
        };
        try {
            const token = localStorage.getItem('token');
            const res = await https.post('/getdevicedetailbydeviceid', params);
            if (token) {
                const loadNameStation = res.data.data.data;

                setLoadStation([loadNameStation]);

                const temporaryStatus = {};

                res.data.data.forEach((device) => {
                    device.data.forEach((detail) => {
                        temporaryStatus[detail.device.code] = detail.deviceStatus;
                    });
                });

            }
        } catch (error) {
            console.log('error: ', error);
        }


    };
    useEffect(() => {
        const FetchStations1 = async () => {
            const params = {
                "data": {
                    "deviceid": 12
                }
            };
            try {
                const token = localStorage.getItem('token');
                const res = await https.post('/getdevicedetailbydeviceid', params);
                if (token) {
                    const loadNameStation = res.data.data.data;

                    setLoadStation1([loadNameStation]);



                }
            } catch (error) {
                console.log('error: ', error);
            }


        };
        FetchStations1()
        const intervalId = setInterval(() => {
            FetchStations1();
        }, 5000);

        return () => {
            clearInterval(intervalId);
        };
    }, [])


    const ControlDevice = async (code, status) => {
        const params = {
            "data": {
                "code": code,
                "status": status
            }
        };

        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiOnOffControl, params);
            if (token) {



            }
        } catch (error) {
            console.log('error: ', error);
        }
    };
    const statusMap = {
        OFF: `${t('control.off')}`,
        DISCONNECT: `${t('control.disconnect')}`,
        ON: `${t('control.on')}`
    };
    const handleCardClick = (code, status) => {
        let confirmationMessage = '';

        if (status === 'ON') {
            confirmationMessage = `${t('onoff.pumpOn')}`;
        } else if (status === 'OFF') {
            confirmationMessage = <span style={{ color: 'blue' }}>This is rush hour! {t('onoff.pumpOff')} </span>;
        } else if (status === 'DISCONNECT') {
            confirmationMessage = `${t('onoff.pumpDis')}`;
        }

        const modalConfig = {
            content: confirmationMessage,
            okText: <span style={{ color: 'white' }}>{t('edit.confirm')}</span>,
            cancelText: `${t('edit.cancel')}`,
            onOk: async () => {
                if (status === 'ON') {
                    await ControlDevice(code, 'OFF');
                    // Cập nhật trạng thái mới trong loadStation
                    setLoadStation((prevLoadStation) =>
                        prevLoadStation.map((item) => {
                            if (item.device.code === code) {
                                return { ...item, deviceStatus: 'OFF' };
                            }
                            return item;
                        })
                    );
                } else if (status === 'OFF') {
                    await ControlDevice(code, 'ON');
                    // Cập nhật trạng thái mới trong loadStation
                    setLoadStation((prevLoadStation) =>
                        prevLoadStation.map((item) => {
                            if (item.device.code === code) {
                                return { ...item, deviceStatus: 'ON' };
                            }
                            return item;
                        })
                    );
                }
            },
        };

        if (status === 'DISCONNECT') {
            modalConfig.okButtonProps = { hidden: true };
        }

        Modal.confirm(modalConfig);
    };

    useEffect(() => {
        FetchStations();
        ControlDevice()
    }, []);



    const handleSwitchChange = (checked, deviceCode, deviceStatus) => {
        handleCardClick(deviceCode, deviceStatus);
    };

    const fetchMonitoring = async () => {
        if (!startDate || !endDate) return

        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "deviceid": 12,
                "thongsoid": 3
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post("/getdevicemonitoringdemo", params);
            if (token) {
                setParameter(res.data.data.datatable);
                setNameValue(res.data.data.datatable[0].parameter)
                const lastItem = res.data.data.datatable[res.data.data.datatable.length - 1];
                setLastValue(lastItem ? lastItem.value : 0);
                const lastTime = res.data.data.datatable[res.data.data.datatable.length - 1];
                setTimeStamp(lastTime ? lastTime.timeStamp.slice(0, 16) : 0);

            }

        } catch (error) {
            console.log('error: ', error);
        }
        setTimeout(fetchMonitoring, 60 * 1000)

    }
    const options = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0,
            },
        },
        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    enabled: false,
                    position: "top",
                    style: {
                        fontSize: "14px",
                        fontWeight: "bold",
                        colors: [""],
                    },
                },
            },
        },
        xaxis: {
            categories: parameter.map((data) => {
                return data.timeStamp.slice(5, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 40
        },
        legend: {
            show: false,
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 4]
        },
        colors: ['#EF4444'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },
        dataLabels: {
            enabled: false,
        }
    };
    const optionsMB = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0,
            },
        },
        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    enabled: false,
                    position: "top",
                    style: {
                        fontSize: "14px",
                        fontWeight: "bold",
                        colors: [""],
                    },
                },
            },
        },
        xaxis: {
            categories: parameter.map((data) => {
                return data.timeStamp.slice(5, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 40
        },
        legend: {
            show: false,
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 4]
        },
        colors: ['#EF4444'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },
        dataLabels: {
            enabled: false,
        }
    };
    const series = [

        {
            name: nameValue.code,
            data: parameter.map((item) => item.value),
            strokeWidth: 1,
        },

    ]
    const optionGauge = {
        chart: {
            height: 350,
            type: 'radialBar',
            toolbar: {
                show: true
            }
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 225,
                minAngle: 0, // Giá trị min là -135 độ
                maxAngle: 15, // Giá trị max là 135 độ
                hollow: {
                    margin: 0,
                    size: '70%',
                    background: '#fff',
                    image: undefined,
                    imageOffsetX: 0,
                    imageOffsetY: 0,
                    position: 'front',
                    dropShadow: {
                        enabled: true,
                        top: 3,
                        left: 0,
                        blur: 4,
                        opacity: 0.24
                    }
                },
                track: {
                    background: '#fff',
                    strokeWidth: '67%',
                    margin: 0,
                    dropShadow: {
                        enabled: true,
                        top: -3,
                        left: 0,
                        blur: 4,
                        opacity: 0.35
                    }
                },

                dataLabels: {
                    show: true,
                    name: {
                        offsetY: -20,
                        show: true,
                        color: '#888',
                        fontSize: '15px'
                    },
                    value: {
                        offsetY: 10,

                        color: '#111',
                        fontSize: '36px',
                        show: true,
                        formatter: function (val) {
                            return val.toString();
                        }
                    }
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 1,
                gradientToColors: ['#f5af19'],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        stroke: {
            lineCap: 'round'
        },
        labels: ['Temperature (°C)'],
    };
    const optionGaugeMB = {
        chart: {
            height: 350,
            type: 'radialBar',
            toolbar: {
                show: true
            }
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 225,
                minAngle: 0, // Giá trị min là -135 độ
                maxAngle: 15, // Giá trị max là 135 độ
                hollow: {
                    margin: 0,
                    size: '70%',
                    background: '#fff',
                    image: undefined,
                    imageOffsetX: 0,
                    imageOffsetY: 0,
                    position: 'front',
                    dropShadow: {
                        enabled: true,
                        top: 3,
                        left: 0,
                        blur: 4,
                        opacity: 0.24
                    }
                },
                track: {
                    background: '#fff',
                    strokeWidth: '67%',
                    margin: 0,
                    dropShadow: {
                        enabled: true,
                        top: -3,
                        left: 0,
                        blur: 4,
                        opacity: 0.35
                    }
                },

                dataLabels: {
                    show: true,
                    name: {
                        offsetY: 0,
                        show: true,
                        color: '#888',
                        fontSize: '10px'
                    },
                    value: {
                        offsetY: 5,

                        color: '#111',
                        fontSize: '15px',
                        show: true,
                        formatter: function (val) {
                            return val.toString();
                        }
                    }
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 1,
                gradientToColors: ['#f5af19'],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        stroke: {
            lineCap: 'round'
        },
        labels: ['Temperature (°C)'],
    };
    const seriesGauge = [lastValue];

    const exportToExcel = () => {
        const workbook = XLSX.utils.book_new();
        const data = parameter.map((data, index) => ({
            Date: parameter[index].timeStamp.slice(0, 10),
            times: parameter[index].timeStamp.slice(11, 16),
            [nameValue.code]: series[0].data[index],
        }));
        const worksheet = XLSX.utils.json_to_sheet(data);

        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, `${t('monitoring.title')} (${nameValue.code})   ${startDate} - ${endDate}  .xlsx`);
    };

    const fetchMonitoring1 = async () => {
        if (!startDate || !endDate) return

        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "deviceid": 12,
                "thongsoid": 4
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post("/getdevicemonitoringdemo", params);
            if (token) {
                setParameter1(res.data.data.datatable);
                setNameValue1(res.data.data.datatable[0].parameter)
                const lastItem = res.data.data.datatable[res.data.data.datatable.length - 1];
                setLastValue1(lastItem ? lastItem.value : 0);
            }
            setTimeout(fetchMonitoring1, 60 * 1000);

        } catch (error) {
            console.log('error: ', error);
        }

    }
    const options1 = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0,
            },
        },

        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    dataLabels: {
                        enabled: false,
                        position: "top",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold",
                            colors: [""],
                        },
                    },
                },
            },
        },
        xaxis: {
            categories: parameter1.map((data) => {
                return data.timeStamp.slice(5, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 15
        },

        legend: {
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 0]
        },
        colors: ['#0E9CFF', '#ffc107'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },



    };
    const options1MB = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0,
            },
        },

        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    dataLabels: {
                        enabled: false,
                        position: "top",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold",
                            colors: [""],
                        },
                    },
                },
            },
        },
        xaxis: {
            categories: parameter1.map((data) => {
                return data.timeStamp.slice(5, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 15
        },

        legend: {
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 0]
        },
        colors: ['#0E9CFF', '#ffc107'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },



    };
    const series1 = [

        {
            name: nameValue1.code,
            data: parameter1.map((item) => item.value),
            strokeWidth: 1,
        },

    ]
    const optionGauge1 = {
        chart: {
            height: 350,
            type: 'radialBar',
            toolbar: {
                show: true
            }
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 225,
                minAngle: -50, // Giá trị min là -135 độ
                maxAngle: 135, // Giá trị max là 135 độ
                hollow: {
                    margin: 0,
                    size: '70%',
                    background: '#fff',
                    image: undefined,
                    imageOffsetX: 0,
                    imageOffsetY: 0,
                    position: 'front',
                    dropShadow: {
                        enabled: true,
                        top: 3,
                        left: 0,
                        blur: 4,
                        opacity: 0.24
                    }
                },
                track: {
                    background: '#fff',
                    strokeWidth: '67%',
                    margin: 0,
                    dropShadow: {
                        enabled: true,
                        top: -3,
                        left: 0,
                        blur: 4,
                        opacity: 0.35
                    }
                },

                dataLabels: {
                    show: true,
                    name: {
                        offsetY: -20,
                        show: true,
                        color: '#888',
                        fontSize: '15px'
                    },
                    value: {
                        offsetY: 10,

                        color: '#111',
                        fontSize: '36px',
                        show: true,
                        formatter: function (val) {
                            return val.toString();
                        }
                    }
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 0.5,
                gradientToColors: ['#f5af19',],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        stroke: {
            lineCap: 'round'
        },
        labels: ['Pressure (bar)'],
    };
    const optionGauge1MB = {
        chart: {
            height: 350,
            type: 'radialBar',
            toolbar: {
                show: true
            }
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 225,
                minAngle: -50, // Giá trị min là -135 độ
                maxAngle: 135, // Giá trị max là 135 độ
                hollow: {
                    margin: 0,
                    size: '70%',
                    background: '#fff',
                    image: undefined,
                    imageOffsetX: 0,
                    imageOffsetY: 0,
                    position: 'front',
                    dropShadow: {
                        enabled: true,
                        top: 3,
                        left: 0,
                        blur: 4,
                        opacity: 0.24
                    }
                },
                track: {
                    background: '#fff',
                    strokeWidth: '67%',
                    margin: 0,
                    dropShadow: {
                        enabled: true,
                        top: -3,
                        left: 0,
                        blur: 4,
                        opacity: 0.35
                    }
                },
                dataLabels: {
                    show: true,
                    name: {
                        offsetY: 0,
                        show: true,
                        color: '#888',
                        fontSize: '10px'
                    },
                    value: {
                        offsetY: 5,

                        color: '#111',
                        fontSize: '15px',
                        show: true,
                        formatter: function (val) {
                            return val.toString();
                        }
                    }
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 0.5,
                gradientToColors: ['#f5af19',],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        stroke: {
            lineCap: 'round'
        },
        labels: ['Pressure (bar)'],
    };
    const seriesGauge1 = [lastValue1];

    const exportToExcel1 = () => {
        const workbook = XLSX.utils.book_new();
        const data = parameter1.map((data, index) => ({
            Date: parameter1[index].timeStamp.slice(0, 10),
            times: parameter1[index].timeStamp.slice(11, 16),
            [nameValue1.code]: series1[0].data[index],
        }));
        const worksheet = XLSX.utils.json_to_sheet(data);

        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, `${t('monitoring.title')} (${nameValue1.code})   ${startDate} - ${endDate}  .xlsx`);
    };

    const handleStartDateChange = (date, dateString) => {
        setStartDate(dateString);
    };

    const handleEndDateChange = (date, dateString) => {
        setEndDate(dateString);
    };

    useEffect(() => {
        fetchMonitoring()
        fetchMonitoring1()
    }, [endDate, startDate,])

    return (

        <div>
            <div className='PC-RES'>
                <div className='flex py-2'  >
                    <div style={{ width: 20 }}></div>
                    <div className=' bg-white responsive_select ' style={{ borderRadius: 5, display: 'flex', justifyContent: 'space-between', padding: '0px 10px', width: '100%' }}>
                        <div className=' flex  ' style={{ alignItems: 'center', }}>
                            <p className='px-2  ' style={{ fontSize: 17, color: '#036E9B', fontWeight: 500 }} >ISI  </p>
                            <p>/</p>
                            <Select
                                bordered={false}
                                placeholder={<span className='text-black'>OGA Station</span>}
                            >
                                <Select.Option>

                                    <div className='flex' style={{ alignItems: 'center' }}>
                                        <AiFillHome size={17} />
                                        <p className='px-2'>OGA Station</p>
                                    </div>
                                </Select.Option>
                            </Select>
                            <div className=' bg-white  ' style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto' }}>
                                < DatePicker
                                    style={{ marginRight: 5 }}
                                    onChange={handleStartDateChange}
                                    defaultValue={dayjs()}
                                    format={dateFormat}
                                />
                                <DatePicker
                                    onChange={handleEndDateChange}
                                    defaultValue={dayjs()}
                                    format={dateFormat}
                                />
                            </div>
                        </div>
                    </div>
                    <div style={{ width: 20 }}></div>

                </div>

                <div className='flex py-2'>
                    <div style={{ width: 20 }} ></div>

                    <div className='flex ' style={{ width: '70%', }}  >
                        <div className='bg-white' style={{ borderRadius: 5, padding: 10, width: '50%', height: 150 }} >
                            <p className='flex py-1' style={{ fontSize: 18, fontWeight: 500, color: '#555F7E' }}>Control</p>

                            <div className='bg-gray-500 py-2' style={{ justifyContent: 'space-between', width: '60%', display: 'flex', height: '90px', alignItems: 'flex-start', alignSelf: 'stretch', borderRadius: 10, background: '#D9D9D9', boxShadow: '0px 4px 32px 0px rgba(51, 38, 174, 0.04);' }}>


                                <div style={{ display: 'flex', padding: 10, flexDirection: 'column', alignItems: 'center', gap: '10px', }}>
                                    <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M45.8333 27.0834V43.7501H41.6666V39.5834H34.5416C32.9375 43.2501 29.2708 45.8334 25 45.8334C20.7291 45.8334 17.0625 43.2501 15.4583 39.5834H8.33329V43.7501H4.16663V27.0834H8.33329V31.2501H15.4791C16.5208 28.8542 18.4375 26.9376 20.8333 25.8751V22.9167H16.6666V18.7501H33.3333V22.9167H29.1666V25.8751C31.5625 26.9376 33.4791 28.8542 34.5208 31.2501H41.6666V27.0834H45.8333ZM35.4166 4.16675H14.5833C13.4375 4.16675 12.5 5.10425 12.5 6.25008C12.5 7.39591 13.4375 8.33341 14.5833 8.33341H20.8333V10.4167H22.9166V16.6667H27.0833V10.4167H29.1666V8.33341H35.4166C36.5625 8.33341 37.5 7.39591 37.5 6.25008C37.5 5.10425 36.5625 4.16675 35.4166 4.16675Z" fill="#036E9B" />
                                    </svg>


                                </div>
                                <div>
                                    <p style={{ color: '#555F7E', textAlign: 'center', fontSize: 15, fontWeight: 400, }}>Actuator Valve</p>

                                </div>
                                <div style={{ justifyContent: 'flex-end', margin: 'auto', display: 'flex', }}>
                                    {loadStation.map((item) => (
                                        <div key={item.device.id}>
                                            <Switch
                                                defaultChecked
                                                className='gray-switch'
                                                checked={item.deviceStatus === 'ON'}
                                                onChange={(checked) => handleSwitchChange(checked, item.device.code, item.deviceStatus)}
                                                disabled={item.deviceStatus === 'DISCONNECT'}
                                            />
                                            <p className='py-2'>{statusMap[item.deviceStatus] || item.deviceStatus}</p>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                        <div style={{ width: 20 }} ></div>

                        <div className='bg-white' style={{ borderRadius: 5, padding: 10, width: '50%', height: 150 }} >
                            <p className='flex py-1' style={{ fontSize: 18, fontWeight: 500, color: '#555F7E' }}>Status</p>
                            <div className='flex ' style={{ justifyContent: 'space-between', alignItems: 'center', }} >
                                <div style={{ width: '5%' }}></div>
                                <div style={{ background: '#D9D9D9', height: '90px', width: '40%', borderRadius: 10 }}>
                                    <p className='py-2' style={{ fontSize: 15, color: '#555F7E' }}>Actuator Position</p>
                                    {loadStation1.map((item) => (
                                        <div key={item.device.id}>
                                            <p className=''>
                                                {item.deviceStatus === 'ON' ? <p style={{ fontSize: 15, color: '#779341' }}  >Open</p> : item.deviceStatus === 'OFF' ? <p style={{ fontSize: 15, color: '#555F7E' }}  >Close</p> : item.deviceStatus}
                                            </p>
                                        </div>
                                    ))}

                                </div>
                                <div style={{ width: '10%' }}></div>

                                <div style={{ background: '#D9D9D9', height: '90px', width: '40%', borderRadius: 10 }}>
                                    <p className='py-2' style={{ fontSize: 15, color: '#555F7E' }}>Level SW Position</p>
                                    {loadStation.map((item) => (
                                        <div key={item.device.id}>
                                            <p className='' style={{ fontSize: 15, color: '#555F7E' }}>
                                                Off
                                            </p>
                                        </div>
                                    ))}

                                </div>
                                <div style={{ width: '5%', }}></div>

                            </div>
                        </div>

                    </div>
                    <div style={{ width: 20 }} ></div>
                    <div className='bg-white' style={{ width: '29%', borderRadius: 5, padding: 10, }} >
                        <p className='flex ' style={{ fontSize: 18, fontWeight: 500, color: '#555F7E' }}>Last Updated</p>
                        <p className='' style={{ fontSize: 20, marginTop: 30 }}>
                            {TimeStamp}

                        </p>


                    </div>
                    <div style={{ width: 20 }} ></div>


                </div>
                <div className='flex py-2 '>
                    <div style={{ width: 20 }} ></div>

                    <div className='bg-white' style={{ borderRadius: 5, width: '70%', margin: 'auto' }}>
                        <div className=" text-titleBorder flex  " style={{ fontWeight: 400, }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >Historical Data - Temperature (°C)
                            </p>
                            <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel}><SiMicrosoftexcel size={20} /></button>

                        </div>
                        <div className='' style={{ borderRadius: "0px 0px 5px 5px" }}>

                            <ReactApexChart options={options} series={series} type="area" height={300} />
                        </div>

                    </div>
                    <div style={{ width: 20 }} ></div>
                    <div className='bg-white' style={{ width: '29%', borderRadius: 5 }}>
                        <p className='flex' style={{ fontWeight: 500, fontSize: 15, letterSpacing: 0.5, padding: 7, marginLeft: 5, }}>Live Data - Temperature (°C)</p>
                        <ReactApexChart
                            options={optionGauge}
                            series={seriesGauge}
                            type="radialBar"
                            height="300"
                        />
                    </div>
                    <div style={{ width: 20 }} ></div>

                </div>

                <div className='flex py-2 '>
                    <div style={{ width: 20 }} ></div>

                    <div className='bg-white' style={{ borderRadius: 5, width: '70%', margin: 'auto' }}>
                        <div className=" text-titleBorder flex  " style={{ fontWeight: 400, }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >Historical Data - Pressure (Bar)
                            </p>
                            <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel1}><SiMicrosoftexcel size={20} /></button>

                        </div>
                        <div className='' style={{ borderRadius: "0px 0px 5px 5px" }}>

                            <ReactApexChart options={options1} series={series1} type="line" height={300} />
                        </div>

                    </div>
                    <div style={{ width: 20 }} ></div>
                    <div className='bg-white' style={{ width: '29%', borderRadius: 5 }}>
                        <p className='flex' style={{ fontWeight: 500, fontSize: 15, letterSpacing: 0.5, padding: 7, marginLeft: 5, }}>Live Data - Pressure (bar)</p>
                        <ReactApexChart
                            options={optionGauge1}
                            series={seriesGauge1}
                            type="radialBar"
                            height="300"
                        />
                    </div>
                    <div style={{ width: 20 }} ></div>

                </div>
            </div >

            <div className='MOBILE-RES' style={{ padding: 10, }}>

                <div className='flex '   >
                    <div style={{ width: 10 }}></div>
                    <div className=' bg-white responsive_select ' style={{ borderRadius: 5, display: 'flex', justifyContent: 'space-between', padding: '0px 10px', width: '100%' }}>
                        <div className=' flex  ' style={{ alignItems: 'center', }}>
                            <p className='px-2  ' style={{ fontSize: 17, color: '#036E9B', fontWeight: 500 }} >ISI  </p>
                            <p>/</p>
                            <Select
                                bordered={false}
                                placeholder={<span className='text-black'>OGA Station</span>}
                            >
                                <Select.Option>
                                    <div className='flex' style={{ alignItems: 'center' }}>
                                        <AiFillHome size={17} />
                                        <p className='px-2'>OGA Station</p>
                                    </div>
                                </Select.Option>
                            </Select>
                            <div className=' bg-white  ' style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: 'auto' }}>
                                < DatePicker
                                    style={{ marginRight: 5 }}
                                    onChange={handleStartDateChange}
                                    defaultValue={dayjs()}
                                    format={dateFormat}
                                />
                                <DatePicker
                                    onChange={handleEndDateChange}
                                    defaultValue={dayjs()}
                                    format={dateFormat}
                                />
                            </div>
                        </div>
                    </div>
                    <div style={{ width: 10 }}></div>
                </div>
                <div className='flex  py-2' style={{ width: '100%', gap: 10 }}  >
                    <div className='bg-white' style={{ borderRadius: 5, padding: 10, width: '50%', height: 125 }} >
                        <p className='flex ' style={{ fontSize: 15, fontWeight: 500, color: '#555F7E' }}>Control</p>

                        <div className='bg-gray-500 py-2' style={{ gap: '10px', width: '100%', display: 'flex', height: '80px', alignSelf: 'stretch', borderRadius: 10, background: '#D9D9D9', boxShadow: '0px 4px 32px 0px rgba(51, 38, 174, 0.04);' }}>


                            <div style={{ display: 'flex', padding: 10, alignItems: 'center', }}>
                                <svg width="40" height="40" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M45.8333 27.0834V43.7501H41.6666V39.5834H34.5416C32.9375 43.2501 29.2708 45.8334 25 45.8334C20.7291 45.8334 17.0625 43.2501 15.4583 39.5834H8.33329V43.7501H4.16663V27.0834H8.33329V31.2501H15.4791C16.5208 28.8542 18.4375 26.9376 20.8333 25.8751V22.9167H16.6666V18.7501H33.3333V22.9167H29.1666V25.8751C31.5625 26.9376 33.4791 28.8542 34.5208 31.2501H41.6666V27.0834H45.8333ZM35.4166 4.16675H14.5833C13.4375 4.16675 12.5 5.10425 12.5 6.25008C12.5 7.39591 13.4375 8.33341 14.5833 8.33341H20.8333V10.4167H22.9166V16.6667H27.0833V10.4167H29.1666V8.33341H35.4166C36.5625 8.33341 37.5 7.39591 37.5 6.25008C37.5 5.10425 36.5625 4.16675 35.4166 4.16675Z" fill="#036E9B" />
                                </svg>
                            </div>
                            <div>
                                <p style={{ color: '#555F7E', fontSize: 12, fontWeight: 400, }}>Actuator Valve</p>
                                {loadStation.map((item) => (
                                    <div className='flex py-2' style={{ justifyContent: 'space-between' }} key={item.device.id}>
                                        <p>{item.deviceStatus}</p>

                                        <Switch
                                            defaultChecked
                                            className='gray-switch'
                                            checked={item.deviceStatus === 'ON'}
                                            onChange={(checked) => handleSwitchChange(checked, item.device.code, item.deviceStatus)}
                                            disabled={item.deviceStatus === 'DISCONNECT'}
                                        />
                                    </div>
                                ))}
                            </div>

                        </div>
                    </div>


                    <div className='bg-white' style={{ borderRadius: 5, padding: 10, width: '50%', height: 125 }} >
                        <p className='flex ' style={{ fontSize: 15, fontWeight: 500, color: '#555F7E' }}>Status</p>
                        <div className='' style={{}} >
                            <div className='' style={{ background: '#D9D9D9', height: '35px', width: '100%', borderRadius: 10, }}>
                                <p className='' style={{ fontSize: 12, color: '#555F7E' }}>Actuator Position</p>
                                {loadStation.map((item) => (
                                    <div key={item.device.id}>
                                        <p className=''>
                                            {item.deviceStatus === 'ON' ? <p style={{ fontSize: 12, color: '#779341' }}  >Open</p> : item.deviceStatus === 'OFF' ? <p style={{ fontSize: 12, color: '#555F7E' }}  >Close</p> : item.deviceStatus}
                                        </p>
                                    </div>
                                ))}

                            </div>
                            <div className='bg-white' style={{ height: '10px' }} ></div>

                            <div style={{ background: '#D9D9D9', height: '35px', width: '100%', borderRadius: 10 }}>
                                <p className='2' style={{ fontSize: 12, color: '#555F7E' }}>Level SW Position</p>
                                {loadStation.map((item) => (
                                    <div key={item.device.id}>
                                        <p className='' style={{ fontSize: 12, color: '#555F7E' }}>
                                            Off
                                        </p>
                                    </div>
                                ))}

                            </div>

                        </div>
                    </div>

                </div>
                <div>
                    <div className='flex' style={{ gap: 10 }}>

                        <div className='bg-white' style={{ width: '50%', borderRadius: 5 }}>
                            <p className='flex' style={{ fontWeight: 500, fontSize: 12, letterSpacing: 0.5, padding: 7, marginLeft: 5, }}>Live Data - Temperature (°C)</p>
                            <ReactApexChart
                                options={optionGaugeMB}
                                series={seriesGauge}
                                type="radialBar"
                                height="170"
                            />
                            <p className='flex  ' style={{ fontSize: 12, alignItems: 'center', padding: '5px' }}>Last Updated : {TimeStamp}</p>

                        </div>
                        <div className='bg-white' style={{ width: '50%', borderRadius: 5 }}>
                            <p className='flex' style={{ fontWeight: 500, fontSize: 12, letterSpacing: 0.5, padding: 7, marginLeft: 5, }}>Live Data - Pressure (bar)</p>
                            <ReactApexChart
                                options={optionGauge1MB}
                                series={seriesGauge1}
                                type="radialBar"
                                height="170"
                            />
                            <p className='flex  ' style={{ fontSize: 12, alignItems: 'center', padding: '5px' }}>Last Updated : {TimeStamp}</p>
                        </div>

                    </div>
                    <div style={{ height: 10 }} ></div>

                    <div className='bg-white' style={{ borderRadius: 5, width: '100%', margin: 'auto', }}>
                        <div className=" text-titleBorder flex  " style={{ fontWeight: 400, }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >Historical Data - Temperature (°C)
                            </p>
                            <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel}><SiMicrosoftexcel size={20} /></button>

                        </div>
                        <div className='' style={{ borderRadius: "0px 0px 5px 5px" }}>

                            <ReactApexChart options={optionsMB} series={series} type="area" height={250} />
                        </div>

                    </div>

                    <div style={{ height: 10 }} ></div>


                    <div className='bg-white py-2' style={{ borderRadius: 5, width: '100%', margin: 'auto' }}>
                        <div className=" text-titleBorder flex  " style={{ fontWeight: 400, }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >Historical Data - Pressure (°C)
                            </p>
                            <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel}><SiMicrosoftexcel size={20} /></button>

                        </div>
                        <div className='' style={{ borderRadius: "0px 0px 5px 5px" }}>

                            <ReactApexChart options={options1MB} series={series1} type="line" height={250} />

                        </div>

                    </div>
                </div>
            </div>
        </div >
    )
}

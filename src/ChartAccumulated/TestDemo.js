import React, { useEffect, useState, useTransition } from 'react';
import ReactApexChart from 'react-apexcharts';
import * as XLSX from 'xlsx';
import { ApiOnOffControl, ApiQuanTrac, https } from '../Service/ConFigURL';
import dayjs from 'dayjs';
import { Card, DatePicker, Modal, Select, Table } from 'antd';
import { AiOutlineHome } from 'react-icons/ai';
import { SiMicrosoftexcel } from 'react-icons/si';

import { useTranslation } from 'react-i18next';
import TestPumps from './TestPumps';
import TemperatureChart from './TemperatureChart';
import PressureChart from './PressureChart';

export default function TestDemo() {
    const dateFormat = 'YYYY-MM-DD';
    const { t, i18n } = useTranslation()

    const [parameter, setParameter] = useState([]);
    const [parameter1, setParameter1] = useState([]);

    const [nameValue, setNameValue] = useState([])
    const [nameValue1, setNameValue1] = useState([])

    const [dataTable, setDataTale] = useState([])
    const [dataTable1, setDataTale1] = useState([])

    const [startDate, setStartDate] = useState(dayjs().format(dateFormat)); // Đặt ngày bắt đầu là ngày hiện tại
    const [endDate, setEndDate] = useState(dayjs().format(dateFormat)); // Đặt ngày kết thúc là ngày hiện tại
    const [selectedThongsoid, setSelectedThongsoid] = useState(3)
    const { Option } = Select;
    const [loadStation, setLoadStation] = useState([]);
    const [loadStation1, setLoadStation1] = useState([]);

    const [loadStatus, setLoadStatus] = useState([]);

    const [temporaryDeviceStatus, setTemporaryDeviceStatus] = useState({});

    //==================================================================================================================

    const fetchMonitoring = async () => {
        if (!startDate || !endDate) return

        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "deviceid": 12,
                "thongsoid": 3
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post("/getdevicemonitoringdemo", params);
            if (token) {
                setParameter(res.data.data.datatable);
                setNameValue(res.data.data.datatable[0].parameter)
                setDataTale(res.data.data.datatable)

            }

        } catch (error) {
            console.log('error: ', error);
        }
        setTimeout(fetchMonitoring, 10 * 60 * 1000)

    }
    const fetchMonitoring1 = async () => {
        if (!startDate || !endDate) return

        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "deviceid": 12,
                "thongsoid": 4
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post("/getdevicemonitoringdemo", params);
            if (token) {
                setParameter1(res.data.data.datatable);
                setNameValue1(res.data.data.datatable[0].parameter)
                setDataTale1(res.data.data.datatable)

            }
            setTimeout(fetchMonitoring1, 10 * 60 * 1000);

        } catch (error) {
            console.log('error: ', error);
        }

    }

    useEffect(() => {
        fetchMonitoring()
        fetchMonitoring1()
    }, [endDate, startDate,])

    const options = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 1,
            },
        },
        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    enabled: false,
                    position: "top",
                    style: {
                        fontSize: "14px",
                        fontWeight: "bold",
                        colors: [""],
                    },
                },
            },
        },
        xaxis: {
            categories: parameter.map((data) => {
                return data.timeStamp.slice(11, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 40
        },
        legend: {
            show: false,
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 4]
        },
        colors: ['#EF4444'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },
        dataLabels: {
            enabled: false,
        }
    };

    const series = [

        {
            name: nameValue.code,
            data: parameter.map((item) => item.value),
            strokeWidth: 1,
        },

    ]
    const columns = [

        {
            title: <span>Station Name</span>,

            dataIndex: "device",
            key: "device",
            render: (device) => <span>{device.name}</span>
        },
        {
            title: <span>{t('monitoringTable.dayCheck')}</span>,
            dataIndex: "timeStamp",
            key: "timeStamp",
            defaultSortOrder: 'ascend', // Sắp xếp mặc định theo thứ tự giảm dần (ngày mới nhất đến cũ nhất)
            sorter: (a, b) => new Date(b.timeStamp) - new Date(a.timeStamp), // Hàm sắp xếp
            render: (timeStamp) => <span>{timeStamp.slice(0, 16)}</span>,
        },


        {
            title: "Temperature",
            dataIndex: "value",
            key: "value",
        },

        {
            title: <span>{t('monitoringTable.status')}</span>,
            fixed: 'right',

            dataIndex: "valueBool",
            key: "valueBool",
            render: (text) => {
                if (text === false) {
                    return <p className='bg-red-500 text-white  text-center' style={{ borderRadius: 3, padding: 5 }} >{t('monitoringTable.unsatisfactory')}</p>
                } else if (text === true) {
                    return <p className='bg-green-500 text-center  text-white' style={{ borderRadius: 3, padding: 5 }} > {t('monitoringTable.qualified')}</p>
                }
            }
        },
    ];
    const exportToExcel = () => {
        const workbook = XLSX.utils.book_new();
        const data = parameter.map((data, index) => ({
            Date: parameter[index].timeStamp.slice(0, 10),
            times: parameter[index].timeStamp.slice(11, 16),
            [nameValue.code]: series[0].data[index],
        }));
        const worksheet = XLSX.utils.json_to_sheet(data);

        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, `${t('monitoring.title')} (${nameValue.code})   ${startDate} - ${endDate}  .xlsx`);
    };
    const exportToExcel1 = () => {
        const workbook = XLSX.utils.book_new();
        const data = parameter1.map((data, index) => ({
            Date: parameter1[index].timeStamp.slice(0, 10),
            times: parameter1[index].timeStamp.slice(11, 16),
            [nameValue1.code]: series[0].data[index],
        }));
        const worksheet = XLSX.utils.json_to_sheet(data);

        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, `${t('monitoring.title')} (${nameValue1.code})  ${startDate} - ${endDate}  .xlsx`);
    };
    const options1 = {
        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 1,
            },
        },

        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    dataLabels: {
                        enabled: false,
                        position: "top",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold",
                            colors: [""],
                        },
                    },
                },
            },
        },
        xaxis: {
            categories: parameter1.map((data) => {
                return data.timeStamp.slice(11, 16)
            }),
        },
        yaxis: {
            show: true,
            min: 0,
            max: 15
        },

        legend: {
            position: 'top',
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [0, 0]
        },
        colors: ['#0E9CFF', '#ffc107'],
        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",
            },
        },



    };

    const series1 = [
        {
            name: nameValue1.name,
            data: parameter1.map((item) => item.value),
            strokeWidth: 1,
        },
    ]

    const columns1 = [
        {
            title: <span>Station Name</span>,
            dataIndex: "device",
            key: "device",
            render: (device) => <span>{device.name}</span>
        },
        {
            title: <span>{t('monitoringTable.dayCheck')}</span>,
            dataIndex: "timeStamp",
            key: "timeStamp",
            defaultSortOrder: 'ascend', // Sắp xếp mặc định theo thứ tự giảm dần (ngày mới nhất đến cũ nhất)
            sorter: (a, b) => new Date(b.timeStamp) - new Date(a.timeStamp), // Hàm sắp xếp
            render: (timeStamp) => <span>{timeStamp.slice(0, 16)}</span>,
        },

        {
            title: "Pressure",
            dataIndex: "value",
            key: "value",
        },
        {
            title: <span>{t('monitoringTable.status')}</span>,
            fixed: 'right',

            dataIndex: "valueBool",
            key: "valueBool",
            render: (text) => {
                if (text === false) {
                    return <p className='bg-red-500 text-white  text-center' style={{ borderRadius: 3, padding: 5 }} >{t('monitoringTable.unsatisfactory')}</p>
                } else if (text === true) {
                    return <p className='bg-green-500 text-center  text-white' style={{ borderRadius: 3, padding: 5 }} >{t('monitoringTable.qualified')} </p>
                }
            }
        },
    ];
    const handleStartDateChange = (date, dateString) => {

        setStartDate(dateString);

    };

    const handleEndDateChange = (date, dateString) => {

        setEndDate(dateString);

    };


    return (
        <div>
            {/* <div className=' bg-white responsive_select  ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                <div className=' flex  py-2 ' style={{ alignItems: 'center', }}>
                    <AiOutlineHome size={20} />
                    <a className='px-1 ' style={{ fontSize: 14 }} >Home  </a>
                    <p className='px-2'>/</p>
                    <p >Dashboard</p>

                </div>
            </div>
            <div style={{ position: 'relative', top: 15 }}>
                <TestPumps />
            </div>

            <div>
                <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                    <div className=' flex  ' style={{ alignItems: 'center', padding: 10 }}>
                        < DatePicker
                            style={{ marginRight: 5 }}
                            onChange={handleStartDateChange}
                            defaultValue={dayjs()}
                            format={dateFormat}
                        />
                        <DatePicker
                            onChange={handleEndDateChange}
                            defaultValue={dayjs()}
                            format={dateFormat}
                        />
                    </div>
                </div>
                <div className='flex '>
                    <div style={{ width: '50%' }} className='py-1'>
                        <div className='flex'>
                            <div style={{ width: '1%' }} ></div>
                            <div className='' style={{ width: '98%', borderRadius: 5 }}>
                                <div className=" text-titleBorder flex bg-white " style={{ fontWeight: 400, }} >
                                    <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('monitoring.title')} ({nameValue.code})
                                    </p>
                                    <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel}><SiMicrosoftexcel size={20} /></button>

                                </div>
                                <div className='bg-white' style={{ borderRadius: "0px 0px 5px 5px" }}>
                                    <ReactApexChart options={options} series={series} type="area" height={350} />
                                </div>
                                <div style={{ width: '100%' }} className='py-3' >
                                    <div className='bg-white ' style={{ borderRadius: 5, width: "100%" }}>
                                        <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>{t('monitoring.titleTable')} ({nameValue.code})
                                            </p>
                                        </div>
                                    </div>
                                    <div className='bg-white'>
                                        <Table
                                            scroll={{ x: 'max-content', }}
                                            dataSource={dataTable} pagination={{
                                                pageSize: 5,
                                            }} columns={columns} />
                                    </div>
                                </div>
                            </div>
                            <div style={{ width: '1%' }} ></div>
                        </div>
                    </div>
                    <div style={{ width: '50%' }} className='py-1'>
                        <div className='flex'>
                            <div style={{ width: '1%' }} ></div>
                            <div className='' style={{ width: '98%', borderRadius: 5 }}>
                                <div className=" text-titleBorder flex bg-white " style={{ fontWeight: 400, }} >
                                    <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('monitoring.title')} ({nameValue1.code})
                                    </p>
                                    <button className=' mx-2 px-2 hover:bg-gray-400 hover:text-white hoverbutton-excel ' style={{ borderRadius: 2, justifyContent: 'flex-end', marginLeft: 'auto', }} onClick={exportToExcel1}><SiMicrosoftexcel size={20} /></button>

                                </div>
                                <div className='bg-white' style={{ borderRadius: "0px 0px 5px 5px" }}>
                                    <ReactApexChart options={options1} series={series1} type="line" height={350} />
                                </div>
                                <div style={{ width: '100%' }} className='py-3' >
                                    <div className='bg-white ' style={{ borderRadius: 5, width: "100%" }}>
                                        <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>{t('monitoring.titleTable')} ({nameValue1.code})
                                            </p>
                                        </div>
                                    </div>
                                    <div className=' bg-white'>
                                        <Table
                                            scroll={{ x: 'max-content', }}

                                            dataSource={dataTable1} pagination={{
                                                pageSize: 5,
                                            }} columns={columns1} />
                                    </div>
                                </div>
                            </div>
                            <div style={{ width: '1%' }} ></div>
                        </div>
                    </div>



                </div>
            </div> */}
            <TemperatureChart />
            <PressureChart />
        </div>
    );
}

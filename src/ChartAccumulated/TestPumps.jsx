import React, { useEffect, useState } from 'react'
import { ApiOnOffControl, https } from '../Service/ConFigURL';
import { Card, Modal, Switch, Table } from 'antd';
import { useTranslation } from 'react-i18next';
import ReactApexChart from 'react-apexcharts';
import pipe from "./img/pump.svg"
export default function TestPumps() {
    const [loadStation, setLoadStation] = useState([]);


    const { t, i18n } = useTranslation()

    const FetchStations = async () => {
        const params = {
            "data": {
                "deviceid": 12
            }
        };
        try {
            const token = localStorage.getItem('token');
            const res = await https.post('/getdevicedetailbydeviceid', params);
            if (token) {
                const loadNameStation = res.data.data.data;
                const loadNameStation1 = res.data.data.data;

                setLoadStation([loadNameStation]);

                const temporaryStatus = {};

                res.data.data.forEach((device) => {
                    device.data.forEach((detail) => {
                        temporaryStatus[detail.device.code] = detail.deviceStatus;
                    });
                });
            }
        } catch (error) {
            console.log('error: ', error);
        }
    };

    const ControlDevice = async (code, status) => {
        const params = {
            "data": {
                "code": code,
                "status": status
            }
        };

        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiOnOffControl, params);
            if (token) {
            }
        } catch (error) {
            console.log('error: ', error);
        }
    };
    const statusMap = {
        OFF: `${t('control.off')}`,
        DISCONNECT: `${t('control.disconnect')}`,
        ON: `${t('control.on')}`
    };
    const handleCardClick = (code, status) => {
        let confirmationMessage = '';

        if (status === 'ON') {
            confirmationMessage = `${t('onoff.pumpOn')}`;
        } else if (status === 'OFF') {
            confirmationMessage = <span style={{ color: 'blue' }}>This is rush hour! {t('onoff.pumpOff')} </span>;
        } else if (status === 'DISCONNECT') {
            confirmationMessage = `${t('onoff.pumpDis')}`;
        }

        const modalConfig = {
            content: confirmationMessage,
            okText: <span style={{ color: 'white' }}>{t('edit.confirm')}</span>,
            cancelText: `${t('edit.cancel')}`,
            onOk: async () => {
                if (status === 'ON') {
                    await ControlDevice(code, 'OFF');
                    // Cập nhật trạng thái mới trong loadStation
                    setLoadStation((prevLoadStation) =>
                        prevLoadStation.map((item) => {
                            if (item.device.code === code) {
                                return { ...item, deviceStatus: 'OFF' };
                            }
                            return item;
                        })
                    );
                } else if (status === 'OFF') {
                    await ControlDevice(code, 'ON');
                    // Cập nhật trạng thái mới trong loadStation
                    setLoadStation((prevLoadStation) =>
                        prevLoadStation.map((item) => {
                            if (item.device.code === code) {
                                return { ...item, deviceStatus: 'ON' };
                            }
                            return item;
                        })
                    );
                }
            },
        };

        if (status === 'DISCONNECT') {
            modalConfig.okButtonProps = { hidden: true };
        }

        Modal.confirm(modalConfig);
    };


    useEffect(() => {
        FetchStations();
        ControlDevice()

    }, []);
    const handleSwitchChange = (checked, deviceCode, deviceStatus) => {
        handleCardClick(deviceCode, deviceStatus);
    };

    return (
        <div>


            <div className='flex py-2' >
                <div style={{ width: 20 }} ></div>
                <div className='bg-white' style={{ width: '35%', height: 150, borderRadius: 5, padding: 10 }} >
                    <p className='flex py-1' style={{ fontSize: 20, fontWeight: 500, color: '#555F7E' }}>Control</p>

                    <div className='bg-gray-500 py-2' style={{ width: '50%', display: 'flex', height: '90px', alignItems: 'flex-start', alignSelf: 'stretch', borderRadius: 10, background: '#D9D9D9', boxShadow: '0px 4px 32px 0px rgba(51, 38, 174, 0.04);' }}>


                        <div style={{ display: 'flex', padding: 10, flexDirection: 'column', alignItems: 'center', gap: '10px', }}>
                            <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M45.8333 27.0834V43.7501H41.6666V39.5834H34.5416C32.9375 43.2501 29.2708 45.8334 25 45.8334C20.7291 45.8334 17.0625 43.2501 15.4583 39.5834H8.33329V43.7501H4.16663V27.0834H8.33329V31.2501H15.4791C16.5208 28.8542 18.4375 26.9376 20.8333 25.8751V22.9167H16.6666V18.7501H33.3333V22.9167H29.1666V25.8751C31.5625 26.9376 33.4791 28.8542 34.5208 31.2501H41.6666V27.0834H45.8333ZM35.4166 4.16675H14.5833C13.4375 4.16675 12.5 5.10425 12.5 6.25008C12.5 7.39591 13.4375 8.33341 14.5833 8.33341H20.8333V10.4167H22.9166V16.6667H27.0833V10.4167H29.1666V8.33341H35.4166C36.5625 8.33341 37.5 7.39591 37.5 6.25008C37.5 5.10425 36.5625 4.16675 35.4166 4.16675Z" fill="#036E9B" />
                            </svg>


                        </div>
                        <div>
                            <p style={{ color: '#555F7E', textAlign: 'center', fontSize: 18, fontWeight: 400, }}>Actuator Valve</p>

                        </div>
                        <div style={{ justifyContent: 'flex-end', margin: 'auto', display: 'flex' }}>
                            {loadStation.map((item) => (
                                <div key={item.device.id}>
                                    <Switch
                                        defaultChecked
                                        className='gray-switch'
                                        checked={item.deviceStatus === 'ON'}
                                        onChange={(checked) => handleSwitchChange(checked, item.device.code, item.deviceStatus)}
                                        disabled={item.deviceStatus === 'DISCONNECT'}
                                    />
                                    <p className='py-2'>{statusMap[item.deviceStatus] || item.deviceStatus}</p>

                                </div>
                            ))}
                        </div>
                    </div>


                </div>
                <div style={{ width: 20 }} ></div>
                <div className='bg-white' style={{ width: '35%', height: 150, borderRadius: 5, padding: 10 }} >
                    <p className='flex py-1' style={{ fontSize: 20, fontWeight: 500, color: '#555F7E' }}>Status</p>
                    <div className='flex ' style={{ justifyContent: 'space-between', alignItems: 'center', }} >
                        <div style={{ width: '15%' }}></div>
                        <div style={{ background: '#D9D9D9', height: '90px', width: '30%', borderRadius: 10 }}>
                            <p className='py-2' style={{ fontSize: 20, color: '#555F7E' }}>Actuator Position</p>
                            {loadStation.map((item) => (
                                <div key={item.device.id}>
                                    <p className=''>
                                        {item.deviceStatus === 'ON' ? <p style={{ fontSize: 20, color: '#779341' }}  >Open</p> : item.deviceStatus === 'OFF' ? <p style={{ fontSize: 20, color: '#555F7E' }}  >Close</p> : item.deviceStatus}
                                    </p>
                                </div>
                            ))}

                        </div>
                        <div style={{ width: '10%' }}></div>

                        <div style={{ background: '#D9D9D9', height: '90px', width: '30%', borderRadius: 10 }}>
                            <p className='py-2' style={{ fontSize: 20, color: '#555F7E' }}>Level SW Position</p>
                            {loadStation.map((item) => (
                                <div key={item.device.id}>
                                    <p className='' style={{ fontSize: 20, color: '#555F7E' }}>
                                        Off
                                    </p>
                                </div>
                            ))}

                        </div>
                        <div style={{ width: '15%' }}></div>

                    </div>
                </div>
            </div>
        </div >
    )
}

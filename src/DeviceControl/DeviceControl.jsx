import React, { useEffect, useState } from 'react';
import { ApiHistoryControlPumps, ApiLoadDeviceControl, ApiOnOffControl, https, listFarmingArea, startAndlimit } from '../Service/ConFigURL';
import { Card, Modal, Select, Table, Tabs } from 'antd';
import { AiOutlineHome } from 'react-icons/ai';
import TabPane from 'antd/es/tabs/TabPane';
import { useTranslation } from 'react-i18next';

export default function DeviceControl() {

    const { t, i18n } = useTranslation()

    const [loadStation, setLoadStation] = useState([]);
    const [selectedDeviceName, setSelectedDeviceName] = useState(null);
    const [loadHistory, setLoadHistory] = useState([]);
    const [filteredTrambom, setFilteredTrambom] = useState([]);

    const [loadNameFaming, setLoadNameFarming] = useState([]);
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();
    const [filteredData, setFilteredData] = useState([]);
    const [temporaryDeviceStatus, setTemporaryDeviceStatus] = useState({});
    const [deviceStatuses, setDeviceStatuses] = useState({});
    const [activeTabKey, setActiveTabKey] = useState('1');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const token = localStorage.getItem("token");
                const response = await https.post(listFarmingArea, startAndlimit);
                if (token) {
                    setLoadNameFarming(response.data.data.list);
                    if (response.data.data.list.length > 0) {
                        setSelectedFarmingArea(response.data.data.list[0].id);
                    }
                }
            } catch (err) {
                console.log("err: ", err);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        const filteredData = selectedFarmingAreaId
            ? loadStation.filter((item) => item.farmingArea?.id === selectedFarmingAreaId)
            : loadStation;
        setFilteredData(filteredData);

    }, [selectedFarmingAreaId, loadStation]);

    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value);
    }

    const fectStation = async () => {
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiLoadDeviceControl, { data: {} });
            if (token) {
                setLoadStation(res.data.data.list);
                const temporaryStatus = {};
                res.data.data.list.forEach((device) => {
                    device.lstDeviceDetail.forEach((detail) => {
                        temporaryStatus[detail.device.code] = detail.deviceStatus;
                    });
                });
                setTemporaryDeviceStatus(temporaryStatus);
            }
        } catch (error) {
            console.log('error: ', error);
        }
    }

    const fectOnOffControl = async (code, status) => {
        const Params = {
            data: {
                code: code,
                status: status,
            },
        };

        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiOnOffControl, Params);
            if (token) {

                setLoadStation((prevDevices) => {
                    const updatedDevices = prevDevices.map((device) => {
                        const updatedDetail = device.lstDeviceDetail.map((detail) => {
                            if (detail.device.code === code) {
                                return {
                                    ...detail,
                                    deviceStatus: status,
                                };
                            }
                            return detail;
                        });
                        return {
                            ...device,
                            lstDeviceDetail: updatedDetail,
                        };
                    });
                    return updatedDevices;
                });
                setDeviceStatuses((prevStatuses) => ({
                    ...prevStatuses,
                    [code]: status,
                }));
                setFilteredTrambom((prevData) =>
                    prevData.map((item) =>
                        item.code === code ? { ...item, status: status } : item
                    )
                );
            }
        } catch (err) {
            console.log('err: ', err);
        }
    };

    useEffect(() => {
        fectOnOffControl()
        fectStation();
    }, [])

    const fetchHisPumps = async () => {
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiHistoryControlPumps, { data: {} });
            if (token) {
                setLoadHistory(res.data.data.list);
                const filteredTrambom = res.data.data.list.filter((item) => item.trambom === selectedDeviceName);
                setFilteredTrambom(filteredTrambom);
            }
        } catch (error) {
            console.log('error: ', error);
        }
    }

    const handleSelectChangePumps = (value) => {
        setSelectedDeviceName(value);

        const filteredTrambom = loadHistory.filter((item) => item.trambom === value);
        setFilteredTrambom(filteredTrambom);
    };

    useEffect(() => {
        fetchHisPumps()
    }, [deviceStatuses])

    const statusMap = {
        OFF: 'Đang tắt',
        DISCONNECT: 'Mất kết nối',
        ON: 'Đang bật'
    };
    const handleCardClick = (code, status) => {
        let confirmationMessage = '';

        const now = new Date();
        const currentHour = now.getHours();
        const currentMinute = now.getMinutes();
        const dayOfWeek = now.getDay();

        if (status === 'ON') {
            confirmationMessage = 'Bạn chắc chắn muốn tắt trạm?';
        } else if (status === 'OFF') {
            if (
                dayOfWeek >= 1 && dayOfWeek <= 6 &&
                ((currentHour === 9 && currentMinute >= 30) || // Từ 9 giờ 30 phút trở đi
                    (currentHour > 9 && currentHour < 11) || // Từ 10 giờ đến 10 giờ 59 phút
                    (currentHour === 11 && currentMinute <= 30) || // Đến 11 giờ 30 phút
                    (currentHour >= 17 && currentHour < 20))
            ) {
                confirmationMessage = <span style={{ color: 'blue' }}>Đây là giờ <span style={{ fontWeight: 800 }}> cao điểm</span> . Bạn có chắc chắn muốn bật trạm?</span>;
            } else {
                confirmationMessage = 'Bạn chắc chắn muốn bật trạm?';
            }
        } else if (status === 'DISCONNECT') {
            confirmationMessage = 'Xin lỗi hiện tại trạm bơm đang mất kết nối!';
        }

        const modalConfig = {
            content: confirmationMessage,
            okText: <span style={{ color: 'white' }}>Đồng ý</span>,
            cancelText: 'Hủy',
            onOk: async () => {
                if (status === 'ON') {
                    await fectOnOffControl(code, 'OFF');
                } else if (status === 'OFF') {
                    await fectOnOffControl(code, 'ON');
                }
            },
        };

        if (status === 'DISCONNECT') {
            modalConfig.okButtonProps = { hidden: true };
        }
        Modal.confirm(modalConfig);
    };

    const onChangeTab = (key) => {
        setActiveTabKey(key);
    }

    const columnsDevice = [
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.areaName')}</p>,
            dataIndex: "vungnuoi",
            render: (vungnuoi) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{vungnuoi}</p>,
        },
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.userName')}</p>,
            dataIndex: "name",
            render: (text, record) => {
                if (!text) {
                    return <p className='text-blue-600 nuni-font ' style={{ borderRadius: 3 }}>{t('pumpTable.tools')}</p>;
                }
                return text;
            },
        },
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.pumpDay')}</p>,
            dataIndex: "dateLog",
            render: (dateLog) => <p className='nuni-font  '>{dateLog.slice(0, 11)}</p>
        },
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.pumpTime')}</p>,
            dataIndex: "dateLog",
            render: (dateLog) => <p className='nuni-font  '>{dateLog.slice(11, 16)}</p>
        },
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.nameStation')}</p>,
            dataIndex: "trambom",
            render: (trambom) => <p className='nuni-font  '>{trambom.slice(0, 11)}</p>
        },
        {
            title: <p style={{}} className='nuni-font  ' >{t('pumpTable.status')}</p>,
            dataIndex: "status",
            key: "status",
            render: (text) => {
                if (text === "ON") return <p className='bg-green-500 text-white w-24 h-6 text-center nuni-font' style={{ borderRadius: 3 }} >ON</p>;
                else if (text === "OFF") return <p className='bg-gray-500 text-center w-24 h-6 text-white nuni-font' style={{ borderRadius: 3 }} >OFF</p>
                else if (text === "DISCONNECT") return <p className='bg-gray-500 text-center w-24 h-6 text-white nuni-font' style={{ borderRadius: 3 }} >DISCONNECT   </p>
            }
        },
    ];

    return (
        <div className=''>
            <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                <div className=' flex ' style={{ fontWeight: 400, fontSize: 15, alignItems: 'center', }}>
                    <AiOutlineHome size={20} />
                    <a className='px-2' >Home  </a>
                    <p>/</p>
                    {activeTabKey === '1' && (
                        <Select
                            bordered={false}
                            value={selectedFarmingAreaId}
                            onChange={handleSelectChange}
                            placeholder={<span className='text-black'>{t('select.farmingArea')}</span>}

                        >
                            {loadNameFaming.map((item) => (
                                <Select key={item.id} value={item.id}>
                                    <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#', textAlign: 'center' }}>
                                        {item.name}
                                    </div>
                                </Select>
                            ))}
                        </Select>
                    )}
                    {activeTabKey === '2' && (
                        <div>
                            <Select
                                bordered={false}
                                value={selectedFarmingAreaId}
                                onChange={handleSelectChange}
                                placeholder={<span className='text-black'>{t('select.pond')}</span>}

                            >
                                {loadNameFaming.map((item) => (
                                    <Select key={item.id} value={item.id}>
                                        <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#', textAlign: 'center' }}>
                                            {item.name}
                                        </div>
                                    </Select>
                                ))}
                            </Select>
                            <Select
                                value={selectedDeviceName}
                                onChange={handleSelectChangePumps}
                                bordered={false}
                                placeholder={<span className='text-black'>{t('select.pond')}</span>}


                            >
                                {filteredData.map((station) =>
                                    station.lstDeviceDetail.map((detail) => (
                                        <Select.Option key={detail.device.id} value={detail.device.name}>
                                            <p  >
                                                {detail.device.name}

                                            </p>
                                        </Select.Option>
                                    ))
                                )}
                            </Select>
                        </div>
                    )}
                </div>
            </div>




            <Tabs onChange={onChangeTab} style={{ marginTop: 5, background: 'white' }} >

                <TabPane key='1' tab={<span style={{ marginLeft: 20, padding: 10, }}  >{t('menu.controlPumps')}</span>} >
                    <div className='flex '  >

                        <div className='' style={{ width: '100%', }}>

                            <div className='flex'>

                                <div className="text-xs   flex " style={{ width: '100%', color: " white", fontWeight: 400, background: "#036E9B", }} >

                                    <p className='px-2' style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} > {t('pump.title')}</p>
                                </div>

                            </div>
                            <div className="card-container ">

                                {filteredData.map((device) =>
                                    device.lstDeviceDetail.map((detail) => (
                                        <Card
                                            key={detail.device.id}
                                            hoverable

                                            className={

                                                detail.deviceStatus === 'OFF' ? 'card-off' :
                                                    detail.deviceStatus === 'ON' ? 'card-on' :
                                                        detail.deviceStatus === 'DISCONNECT' ? 'card-disconnect' : ''
                                            }
                                            onClick={() => handleCardClick(detail.device.code, detail.deviceStatus)}

                                        >
                                            <div  >
                                                <div style={{ padding: '0px 30px ' }}>
                                                    <svg className={`svgIcons  ${detail.deviceStatus === 'OFF' ? 'gray-fill' : ''}`} style={{ margin: 'auto', }} width="50" height="50" viewBox="0 0 26 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path className={`svgIcons ${detail.deviceStatus === 'OFF' ? 'gray-fill' : ''} ${detail.deviceStatus === 'DISCONNECT' ? 'gray-fill' : ''} `} d="M0.5 22.75V15.25H2.375C1.9623 14.0428 1.75114 12.7758 1.75 11.5C1.75 8.51631 2.93526 5.65483 5.04505 3.54505C7.15483 1.43526 10.0163 0.25 13 0.25H25.5V7.75H23.625C24.025 8.925 24.25 10.1875 24.25 11.5C24.25 14.4837 23.0647 17.3452 20.955 19.455C18.8452 21.5647 15.9837 22.75 13 22.75H0.5ZM4.25 11.5C4.25 13.1 4.675 14.5875 5.425 15.875L9.75 13.375C9.4375 12.825 9.25 12.1875 9.25 11.5C9.25 10.6875 9.5125 9.9375 9.95 9.325L5.875 6.4125C4.875 7.85 4.25 9.625 4.25 11.5ZM13 20.25C16.2375 20.25 19.0625 18.4875 20.575 15.875L16.25 13.375C15.6 14.5 14.3875 15.25 13 15.25H12.6375L12.1625 20.2125L13 20.25M13 7.75C14.5125 7.75 15.825 8.65 16.4125 9.95L20.9625 7.875C20.2677 6.34626 19.1472 5.04997 17.7352 4.14112C16.3232 3.23227 14.6792 2.74932 13 2.75V7.75M13 10.25C12.3125 10.25 11.75 10.8125 11.75 11.5C11.75 12.1875 12.3125 12.75 13 12.75C13.6875 12.75 14.25 12.1875 14.25 11.5C14.25 10.8125 13.6875 10.25 13 10.25Z" fill="#036E9B" />
                                                    </svg>
                                                </div>
                                                <p style={{ fontWeight: 800 }} >{detail.device.name}</p>
                                                <a>{statusMap[detail.deviceStatus] || detail.deviceStatus}</a>
                                            </div>

                                        </Card>
                                    ))
                                )}
                            </div>
                        </div>

                    </div>
                </TabPane>
                <TabPane key='2' tab={<span>{t('menu.controlTimes')}</span>}  >
                    <div className='flex ' >
                        <div style={{ width: '100%' }} >
                            <div className="text-xs   flex " style={{ color: " white", fontWeight: 400, background: "#036E9B", }} >

                                <p className='px-2' style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('pump.titleHis')}</p>

                            </div>

                            <Table className='px-2' dataSource={filteredTrambom} columns={columnsDevice} />

                        </div>


                    </div>
                </TabPane>
            </Tabs>
        </div>
    )
}

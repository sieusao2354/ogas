
import React, { useEffect, useState } from 'react'
import ReactApexChart from 'react-apexcharts';
import { ApiChartPump, ApiChartPumpYear, ContentType, https, } from '../Service/ConFigURL';
import { customLocale } from '../chart/DatePickerViVn';
import { DatePicker } from 'antd';
import moment from 'moment'
import dayjs from 'dayjs';
import { AiFillHome, AiOutlineHome } from 'react-icons/ai';
import "./DeviceControl.css"
import { useTranslation } from 'react-i18next';
const monthFormat = 'YYYY';

export default function SystemPump() {
    const { t, i18n } = useTranslation()


    const [loadPump, setLoadPump] = useState([])
    const [loadPump1, setLoadPump1] = useState([])
    const [loadNamePump, setLoadNamePump] = useState([])

    const [loadPumpTime, setLoadPumpTime] = useState([])
    const [loadPump1Time, setLoadPump1Time] = useState([])
    const [loadNamePumpTime, setLoadNamePumpTime] = useState([])


    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()

    const [loadPumpYear, setLoadPumpYear] = useState([])
    const [loadPump1Year, setLoadPump1Year] = useState([])
    const [loadNamePumpYear, setLoadNamePumpYear] = useState([])

    const [selectYear, setSelectYear] = useState(moment().year())


    useEffect(() => {
        Chartpump()
    }, [])
    const currentDates = new Date()
    const endDateChartPond = currentDates.toISOString("en-US")

    const Chartpump = async () => {
        const Params = {
            data: {
                startdate: "2023-01-01",
                enddate: endDateChartPond,
                vungnuoiid: 2
            }
        }
        try {
            const token = localStorage.getItem('token')
            const response = await https.post(ApiChartPump, Params)

            if (token) {
                const lstNumber = response.data.data.lstdatachart;
                const names = lstNumber.map((item) => item.device.name);
                setLoadNamePump(names);

                const dataPump = lstNumber.map((item) => item.lstNumber[0][0])
                setLoadPump(dataPump)
                const dataPump1 = lstNumber.map((item) => item.lstNumber[0][1])
                setLoadPump1(dataPump1)
            }
        }
        catch (err) {
            console.log('err: ', err);

        }
    }

    useEffect(() => {
        ChartpumpTime()
    }, [startDate, endDate])
    const ChartpumpTime = async () => {
        if (!startDate || !endDate) return;
        const Params = {
            data: {
                startdate: startDate,
                enddate: endDate,
                vungnuoiid: 2
            }
        }
        try {
            const token = localStorage.getItem('token')
            const response = await https.post(ApiChartPump, Params)

            if (token) {
                const lstNumber = response.data.data.lstdatachart;
                const names = lstNumber.map((item) => item.device.name);
                setLoadNamePumpTime(names);

                const dataPump = lstNumber.map((item) => item.lstNumber[0][0])
                setLoadPumpTime(dataPump)
                const dataPump1 = lstNumber.map((item) => item.lstNumber[0][1])
                setLoadPump1Time(dataPump1)
            }
        }
        catch (err) {
            console.log('err: ', err);

        }
    }
    useEffect(() => {
        ChartpumpYear()
    }, [selectYear])
    const ChartpumpYear = async () => {
        const Params = {
            data: {
                year: selectYear,
                vungnuoiid: 2
            }
        }
        try {
            const token = localStorage.getItem('token')
            const response = await https.post(ApiChartPumpYear, Params)

            if (token) {
                const lstNumber = response.data.data.lstdatachart;
                const names = lstNumber.map((item) => item.device.name);
                setLoadNamePumpYear(names);

                const dataPump = lstNumber.map((item) => item.lstNumber[0][0])
                setLoadPumpYear(dataPump)
                const dataPump1 = lstNumber.map((item) => item.lstNumber[0][1])
                setLoadPump1Year(dataPump1)
            }
        }
        catch (err) {
            console.log('err: ', err);

        }
    }
    const handleDatePickerYear = date => {
        setSelectYear(date.year())
    }
    const optionsYear = {
        chart: {
            type: 'bar',
            stacked: false,
        },
        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        }, legend: {
            position: 'top',
        },
        xaxis: {
            categories: loadNamePumpYear
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        colors: ['#429D44', '#036E9B',],
        series: [
            {

                data: loadPumpYear.map(item => item)
            },
            {

                data: loadPump1Year.map(item => item)
            }
        ],
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 200,

            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 0,
                borderWidth: 0,
                borderColor: '',
                opacity: 0.01,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#fff',
                    opacity: 0.45
                }
            },
        },


    }

    const optionsTime = {
        chart: {
            type: 'bar',
            stacked: false,
        },
        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        }, legend: {
            position: 'top',
        },
        xaxis: {
            categories: loadNamePumpTime
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        colors: ['#429D44', '#036E9B',],
        series: [
            {
                data: loadPumpTime.map(item => item)
            },
            {
                data: loadPump1Time.map(item => item)
            }
        ],
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 200,
            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 0,
                borderWidth: 0,
                borderColor: '',
                opacity: 0.01,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#fff',
                    opacity: 0.45
                }
            },
        },
    }

    const options = {
        chart: {
            type: 'bar',
            stacked: false,
        },
        plotOptions: {
            bar: {

                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        }, legend: {
            position: 'top',
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },
        xaxis: {
            categories: loadNamePump
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        colors: ['#429D44', '#036E9B',],
        series: [
            {
                data: loadPump.map(item => item)
            },
            {
                data: loadPump1.map(item => item)
            }
        ],
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 200,
            },
            background: {
                enabled: false,
                foreColor: '#fff',
                padding: 4,
                borderRadius: 0,
                borderWidth: 0,
                borderColor: '',
                opacity: 0.01,
                dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#fff',
                    opacity: 0.45
                }
            },
        },
    }
    const renderSelectPage = () => {
        return <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', zIndex: 1 }}>
            <div className=' flex ' style={{ fontWeight: 400, fontSize: 14, alignItems: 'center', height: 30 }}>

                <AiOutlineHome size={20} />
                <a className='px-2' >Home  </a>
                <p>/</p>
                <p className='px-2'>{t('menu.controlTimes')}</p>
            </div>
        </div>
    }
    const resPc = () => {
        return (
            <div  >
                <div className='flex  container-chart py-3  ' style={{ padding: 10, gap: 5 }}   >

                    <div style={{ width: '100%' }}>
                        <div className=" text-titleBorder flex " style={{ color: " white", background: "#036E9B", }} >

                            <p className='' style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('statisticalPumps.titleAllTimes')}</p>

                        </div>
                        <div style={{ height: 32, background: "white" }} > </div>
                        <div className='bg-white borderChartBottom ' >
                            <ReactApexChart type='bar' options={options} series={options.series} height={300} />
                        </div>

                    </div>
                    <div style={{ width: '100%' }}>
                        <div className="  text-titleBorder flex" style={{ color: " white", background: "#036E9B", }} >

                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('statisticalPumps.titleYear')}{selectYear} </p>

                        </div>
                        <div className='bg-white' >
                            <DatePicker defaultValue={dayjs("2023", monthFormat)} format={monthFormat} picker='year' onChange={handleDatePickerYear} />
                        </div>

                        <div className='bg-white borderChartBottom ' >
                            <ReactApexChart

                                options={optionsYear}
                                series={optionsYear.series}
                                type="bar"
                                height={300}

                            />
                            <div className='bg-white borderChartBottom ' ></div>

                        </div>
                    </div>

                </div>
                <div className='flex '>
                    <div style={{ width: '10px' }} ></div>

                    <div className='bg-white  ' style={{ borderRadius: 5, width: '100%' }}>
                        <div className=" text-titleBorder flex " style={{ color: " white", background: "#036E9B", }} >

                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('statisticalPumps.titleByTime')}</p>

                        </div>
                        <div>
                            <DatePicker.RangePicker
                                style={{ position: 'relative', top: 5 }}
                                value={[startDate, endDate]}
                                onChange={(dates) => {
                                    setStartDate(dates[0]);
                                    setEndDate(dates[1]);
                                }}
                                locale={customLocale}
                            />
                        </div>
                        <ReactApexChart
                            options={optionsTime}
                            series={optionsTime.series}
                            type="bar"
                            height={300}
                        />
                    </div>
                    <div style={{ width: '10px' }} ></div>
                </div>
            </div>
        )
    }


    return (

        <div className='font-googleFood' style={{ height: '100vh' }} >
            {renderSelectPage()}
            {resPc()}

        </div>
    )
}
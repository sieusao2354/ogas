import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import enTranslation from './locales/en.json';
import viTranslation from './locales/vi.json';

const resources = {
    en: { translation: enTranslation },
    vi: { translation: viTranslation }
};

i18n.use(initReactI18next).init({
    resources,
    lng: 'en', // Ngôn ngữ mặc định
    keySeparator: false, // Không sử dụng dấu chấm (.) trong các key
    interpolation: {
        escapeValue: false // Không cần escape các giá trị trong chuỗi
    }
});

export default i18n;

import { HashRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import LoginPage from './Page/LoginPage/LoginPage';
import ChartMenuAcc from './ChartAccumulated/ChartMenuAcc';
import AddNewListMobile from './ChartReport/AddNewListMobile';

import './i18next';
function App() {



  return (
    <div className="App" style={{ backgroundColor: '#e5e7eb', }}>

      <HashRouter basename='/'>
        <Routes>

          <Route path='/' element={<LoginPage />} />
          <Route path='/Dashboard' element={<ChartMenuAcc />} />
          <Route path='CreateNewList' element={<AddNewListMobile />} />

        </Routes>
      </HashRouter>



    </div>
  );
}

export default App;

import axios from "axios";
export const BaseUrlLogin = "https://easyfeed.vn/saqa-api/v1/login"; // API login
export const ContentType = "application/json; charset=utf-8"; //Content Type

export const listFarmingArea = '/farmingareas'; //Api load vùng nuôi
export const startAndlimit = { // params của API load vùng nuôi
    "start": 0, "limit": 0
}
export const ApiUpdateArea = "/farmingarea/update"; // API cập nhật và thêm mới vùng nuôi

export const ApiLoadStations = "/stations"; //API load trạm bơm
export const ApiUpdateCreate = "/station/update"; // API cập nhật trạm bơm và thêm mới

export const ApiFeedByDate = "/getfeedbydate"; // API load thức ăn theo kg trên ngày

export const loadFishPond = "/getfishpond"; // API danh sách ao
export const loadFishPondData = { //Params của API danh sách ao
    "data": {
    }
}

export const ApiInsert = "/insertfeed";

export const GetSumMassByYear = "/getsummassbyyear"

export const getSumByDay = "/getsummassbydate"

export const getSumByMonth = "/getsummassbymonth"

export const foodBagByDate = "/getfeedquantitybydate"; // API load thức ăn theo bao trên ngày

export const ApiQuanTrac = "/getdevicemonitoring"; // API quan trắc thống kê pH , DO , Nhiệt độ

export const ApiAccumulatedFishPond = "/getfeedbyperiodtime"; // API lũy kế số bao và kg thức ăn đã sử dụng


export const ApiDevices = "/getdevicecontrol"

export const ApiHistoryControlPumps = "/auditlog"
export const ApiOnOffControl = "/device/control"

export const ApiLoadDeviceControl = "/getdevicecontrol"

export const ApiScaleYear = "/getharvestbymonthinyear"

export const ApiScaleByDay = "/getharvestbydate" // Api chart cân theo ngày 

export const ApiUpdateFeed = "/updatefeed"

export const ApiChartPump = "/getpumpstatistics" // API chart giờ bơm và số lần bơm

export const ApiChartPumpYear = "/getpumpstatisticsbyyear"  // API chart giờ bơm và số lần bơm theo năm 

export const updateListFeed = "/updatefeed"

export const profilesUser = "/profiles"

export const ApiRoles = "/roles"

export const ApiListUserPumps = "/getusers"


export const https = axios.create({ //https của tất cả API 
    baseURL: "https://easyfeed.vn/saqa-api/v1",
})

https.interceptors.request.use( // quét token liên tục
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["token"] = token;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
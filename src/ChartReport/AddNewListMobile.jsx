import React, { useEffect, useState, } from 'react';
import { ApiFeedByDate, ApiInsert, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import { Input, Select, Form, Modal, DatePicker, message, Button } from 'antd';
import dayjs from 'dayjs';
import "./ChartCss.css"
import { AiOutlineHome, AiOutlinePlusCircle } from 'react-icons/ai';
import { AiOutlineInfoCircle, AiOutlineOrderedList } from 'react-icons/ai';
import { MdScale } from 'react-icons/md';
import { TbNumber } from 'react-icons/tb';
import { BsQrCode } from 'react-icons/bs';

import { FcHome } from 'react-icons/fc';
import { FaWater } from 'react-icons/fa';
const AddNewListMobile = () => {
    const [loadNameFaming, setLoadNameFarming] = useState([]);// load vùng nuôi
    const [pondData, setPondData] = useState([]) // load ao nuôi
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();// dùng để liên kết ao và chart 
    const [filteredData, setFilteredData] = useState([]);

    const [selectedFishPondId, setSelectedFishPondId] = useState();
    const [isModalVisible, setIsModalVisible] = useState(false);
    useEffect(() => {

        const fectData = async () => {
            try {
                const token = localStorage.getItem('token')

                const response = await https.post(listFarmingArea, startAndlimit)
                if (token) {
                    setLoadNameFarming(response.data.data.list)
                }
            } catch (error) {
                console.log('error: ', error);
            }
        }
        fectData()
    }, [])

    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value)
    }

    useEffect(() => {
        const loadPond = async () => {
            try {
                const token = localStorage.getItem('token')
                const response = await https.post(loadFishPond, loadFishPondData)
                if (token) {
                    setPondData(response.data.data.list)
                }
            } catch (error) {
                console.log('error: ', error);

            }
        }
        loadPond()
    }, [])

    useEffect(() => {
        if (selectedFarmingAreaId) {
            const filteredPonds = pondData.filter((item) => item.farmingArea && item.farmingArea.id === selectedFarmingAreaId)
            setFilteredData(filteredPonds)
        } else {
            setFilteredData(pondData)
        }
    }, [selectedFarmingAreaId, pondData])

    const handleFishPondSelect = (value) => {
        setSelectedFishPondId(value);
    };

    useEffect(() => {
        setFormData((prevFormData) => ({
            ...prevFormData,
            fishPond: { id: selectedFishPondId }
        }));
    }, [selectedFishPondId]);

    const [formData, setFormData] = useState({
        timeStamp: '',
        barCode: '',
        lotNum: '',
        quantity: '',
        ordinalNum: '',
        dateOfManufacture: '',
        fishPond: {
            id: ''
        }
    });



    const handleAddData = async () => {
        try {
            const response = await https.post(ApiInsert, { data: formData });
            console.log(response.data);

            message.success('Thêm mới thành công ')
        } catch (error) {
            console.log('error: ', error);

        }
    };
    const disabledDateTime = () => ({
        disabledHours: () => range(0, 24).splice(4, 20),
        disabledMinutes: () => range(30, 60),
        disabledSeconds: () => [55, 56],
    });
    const range = (start, end) => {
        const result = [];
        for (let i = start; i < end; i++) {
            result.push(i);
        }
        return result;
    };
    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };
    const handleFormSubmit = (event) => {
        event.preventDefault();
        const updatedFormValues = { ...formData }
        handleAddData(updatedFormValues)
    }


    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = () => {
        // Xử lý logic khi nhấp vào nút "Add" trong modal
        setIsModalVisible(false);
        handleAddData(formData); // Gọi hàm handleAddData từ form gốc
    };
    const handleSaveConfirmation = () => {
        Modal.confirm({
            title: 'Xác nhận',
            content: 'Bạn có chắc chắn muốn tạo danh sách mới?',
            okText: <span style={{ color: 'white' }} >Đồng ý</span>,
            cancelText: 'Hủy bỏ',
            onOk: handleOk,
            centered: true,
            maskClosable: true,
        });
    };

    const handleBarCodeChange = (event) => {
        const { value } = event.target;

        const barCodeParts = value.split(' ');
        const lotNum = barCodeParts[1];
        const quantity = barCodeParts[2];
        const ordinalNum = barCodeParts[3];



        setFormData((prevValues) => ({
            ...prevValues,
            barCode: value,
            lotNum,
            quantity,
            ordinalNum
        }));


        // Tự động cập nhật giá trị của lotNum dựa trên số chỉ mục
        if (quantity === '1') {
            setFormData((prevValues) => ({
                ...prevValues,
                quantity: 40
            }));
        } else if (quantity === '2') {
            setFormData((prevValues) => ({
                ...prevValues,
                quantity: 500
            }));
        } else if (quantity === '3') {
            setFormData((prevValues) => ({
                ...prevValues,
                quantity: 450
            }));
        } else {
            setFormData((prevValues) => ({
                ...prevValues,
                lotNum
            }));
        }
    };

    const styleFormCreate = {
        width: '100%',
    }
    const showModal = () => {
        setIsModalVisible(true);
    };
    return (
        <div>

            <button type="button " className='icons-add-feed flex ' onClick={showModal}><AiOutlinePlusCircle className='button-Create' size={20} style={{ marginTop: 3, }} /> </button>

            <Modal
                className=''
                title={
                    <span>
                        <div className="flex ">
                            {' '}
                            <AiOutlineInfoCircle size={25} style={{ borderRadius: 50, background: 'orange', color: 'white' }} />{' '}
                            <p className="px-2"> Thêm mới danh sách thức ăn</p>
                        </div>
                    </span>
                }
                visible={isModalVisible}
                onCancel={handleCancel}
                onOk={handleSaveConfirmation}
            >

                <div className='' style={{ padding: 20, }} >
                    <Form className='' style={{ display: 'flex ', flexDirection: 'column', gap: 15 }} onSubmit={handleFormSubmit}>
                        <div>

                            <Select
                                style={styleFormCreate}

                                value={selectedFarmingAreaId}
                                onChange={handleSelectChange}
                                defaultValue={"Chọn vùng nuôi"}
                                suffixIcon={<FcHome size={24} />}

                            >
                                {loadNameFaming.map((item) => (
                                    <Select key={item.id} value={item.id}>
                                        <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                            {item.name}
                                        </div>

                                    </Select>
                                ))}
                            </Select>
                        </div>
                        <div>

                            <Select
                                className=''
                                suffixIcon={<FaWater size={24} />}
                                style={styleFormCreate}
                                defaultValue="Chọn Ao" onChange={handleFishPondSelect}
                            >
                                {filteredData.map((item) => (
                                    <Select.Option key={item.id} value={item.id}>
                                        <div className='flex ' style={{ fontWeight: 400, fontSize: 13, color: '#' }}>
                                            {item.name}
                                        </div>
                                    </Select.Option>
                                ))}
                            </Select>
                        </div>

                        <div>

                            <p className=' flex text-blue-600' >Ngày cho ăn</p>
                            <DatePicker
                                style={styleFormCreate}
                                format="YYYY-MM-DD HH:mm:ss"
                                value={formData.timeStamp ? dayjs(formData.timeStamp) : null}
                                disabledTime={disabledDateTime}
                                showTime={{
                                    defaultValue: dayjs('00:00:00', 'HH:mm:ss'),
                                }}
                                onChange={(date, dateString) => {
                                    setFormData({ ...formData, timeStamp: dateString });
                                }}
                            />
                        </div>
                        <p className=' flex text-blue-600' > Ngày sản xuất</p>

                        <div>

                            <DatePicker
                                style={styleFormCreate}

                                format="YYYY-MM-DD HH:mm:ss"
                                value={formData.dateOfManufacture ? dayjs(formData.dateOfManufacture) : null}
                                disabledTime={disabledDateTime}
                                showTime={{
                                    defaultValue: dayjs('00:00:00', 'HH:mm:ss'),
                                }}
                                onChange={(date, dateString) => {
                                    setFormData({ ...formData, dateOfManufacture: dateString });
                                }}
                            />
                        </div>

                        <div>
                            <p className=' flex text-blue-600' > Bar code</p>



                            <Input
                                style={styleFormCreate}
                                prefix={<BsQrCode size={20} />}

                                type="text"
                                name="barCode"
                                value={formData.barCode}
                                onChange={handleBarCodeChange}
                                placeholder="Barcode"
                            />
                        </div>
                        <div>
                            <p className='flex text-blue-600' > Số thứ tự</p>
                            <Input
                                style={styleFormCreate}
                                prefix={<AiOutlineOrderedList size={24} />}


                                type="text"
                                name="ordinalNum"
                                value={formData.ordinalNum}
                                onChange={handleChange}
                                placeholder="Ordinal Number"
                            />
                        </div>
                        <div>
                            <p className=' flex text-blue-600' > Số lượng (kg)</p>

                            <Input
                                style={styleFormCreate}
                                prefix={<MdScale size={24} />}
                                type="text"
                                name="quantity"
                                value={formData.quantity}
                                onChange={handleChange}
                                placeholder="Quantity"
                            />
                        </div>
                        <div>
                            <p className=' flex text-blue-600' > Số lô</p>

                            <Input
                                style={styleFormCreate}

                                prefix={<TbNumber size={24} />}

                                type="text"
                                name="lotNum"
                                value={formData.lotNum}
                                onChange={handleChange}
                                placeholder="Lot Number"
                            />

                        </div>

                    </Form >
                </div>

            </Modal>

        </div >
    );
};

export default AddNewListMobile;


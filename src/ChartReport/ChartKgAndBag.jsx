import React, { useEffect, useState } from 'react'
import { ApiFeedByDate, ApiUpdateFeed, ContentType, foodBagByDate, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import { DatePicker, Form, Input, Modal, Select, Table, Tabs, message } from 'antd';
import ReactApexChart from 'react-apexcharts';
import { AiOutlineHome, AiOutlineInfoCircle, AiOutlineOrderedList } from 'react-icons/ai';
import { MdScale } from 'react-icons/md';
import { TbNumber } from 'react-icons/tb';
import { BsQrCode } from 'react-icons/bs';

import { FcHome } from 'react-icons/fc';
import { FaWater } from 'react-icons/fa';

import dayjs, { Dayjs } from 'dayjs';
import ChartCreate from './ChartCreate';
import "./ChartCss.css"
import AddNewListMobile from './AddNewListMobile';
import { useTranslation } from 'react-i18next';

export default function ChartKgAndBag() {
    const dateFormat = 'YYYY-MM-DD';
    const today = dayjs();
    const { t, i18n } = useTranslation()

    const [loadNameFaming, setLoadNameFarming] = useState([]);// load vùng nuôi
    const [pondData, setPondData] = useState([]) // load ao nuôi
    const [selectedFarmingAreaId, setSelectedFarmingArea] = useState();// dùng để liên kết ao và chart 
    const [filteredData, setFilteredData] = useState([]);

    const [aoid, setAoId] = useState(null);

    const [listQuantities, setListQuantities] = useState([]); // state lấy dữ liệu chart của loadChart
    const startDateKgDefault = today.subtract(15, 'day').format(dateFormat);

    const [startDateKg, setStartDateKg] = useState(startDateKgDefault);
    const [endDateKg, setEndDateKg] = useState(today.format(dateFormat));
    const [nameTableKg, setNameTableKg] = useState([]);

    const [listQuantityAo, setListQuantityAo] = useState([])// state lấy dữ liệu chart của loadPondBag
    const [selectedPondId, setSelectedPondId] = useState(null);

    const [showForm, setShowForm] = useState(false);
    const [formValues, setFormValues] = useState({});
    const [isModalVisible, setIsModalVisible] = useState(false);

    const [selectedRow, setSelectedRow] = useState(null);



    const fetchData = async () => {
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(listFarmingArea, startAndlimit)
            if (token) {
                setLoadNameFarming(response.data.data.list);
                if (response.data.data.list && response.data.data.list.length > 0) {
                    setSelectedFarmingArea(response.data.data.list[0].id);
                }
            }

        } catch (err) {
            console.log("err: ", err);
        }
    };

    const loadPond = async () => {
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(loadFishPond, loadFishPondData);
            if (token) {
                setPondData(response.data.data.list);
            }
        } catch (err) {
            console.log("err: ", err);
        }
    };

    useEffect(() => {
        fetchData()
        loadPond()
    }, [])

    useEffect(() => {// API tìm tên Ao trùng id với chart
        if (selectedFarmingAreaId) {
            const filteredPonds = pondData.filter((item) => item.farmingArea && item.farmingArea.id === selectedFarmingAreaId);
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(pondData);
        }
    }, [selectedFarmingAreaId, pondData]);


    const changeValueCallApiNumber3 = (value) => {
        setAoId(value)
        setSelectedPondId(value);

    };
    const loadChart = async () => {

        if (!startDateKg || !endDateKg || !aoid) return
        const params = {
            data:
            {
                startdate: startDateKg,
                enddate: endDateKg,
                aoid: aoid
            }
        };
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(ApiFeedByDate, params);
            if (token) {
                const lstDate = response.data.data.datachart?.[0]?.lstDate || [];

                if (lstDate.length > 0) {
                    setListQuantities(lstDate);
                    setNameTableKg(response.data.data.datatable);
                } else {
                    setListQuantities([]);
                    setNameTableKg([]);
                }
            }
        } catch (err) {
            console.log("err: ", err);
        }
    };
    useEffect(() => {
        loadChart()
        loadPondBag()

    }, [startDateKg, endDateKg, aoid])

    const loadPondBag = async () => {
        if (!startDateKg || !endDateKg || !aoid) return
        const params = {
            data:
            {
                startdate: startDateKg,
                enddate: endDateKg,
                aoid: aoid
            }
        };
        try {
            const token = localStorage.getItem("token");
            const response = await https.post(foodBagByDate, params);
            if (token) {
                const lstDateAndMap = response?.data?.data?.lstdatachart?.[0]?.lstDateAndMap;

                if (lstDateAndMap && lstDateAndMap.length > 0) {
                    const quantities = lstDateAndMap.map(item => ({
                        date: item.date,
                        "40": item?.mapCountFeed?.["40"],
                        "450": item?.mapCountFeed?.["450"],
                        "500": item?.mapCountFeed?.["500"],
                    }));
                    setListQuantityAo(quantities);
                } else {
                    setListQuantityAo([]);
                }
            }
        } catch (err) {
            console.log("err: ", err);
        }
    };

    const handleStartDateChange = (date, dateString) => {
        setStartDateKg(dateString);
    };

    const handleEndDateChange = (date, dateString) => {
        setEndDateKg(dateString);
    };

    const handleSelectChange = (value) => {
        setSelectedFarmingArea(value)
    }

    const maxDataValue = listQuantities.reduce((max, data) => Math.max(max, data.quantity), 0);
    const yAxisMaxValue = Math.max(maxDataValue, maxDataValue + 1000);
    const numberOfDays = listQuantities.length;
    const showDataLabels = numberOfDays <= 30;
    const dataLabelsOptions = {
        enabled: showDataLabels,
        formatter: function (val) {
            return val;
        },
        offsetY: -20,
        style: {
            fontSize: '12px',
            colors: ["#000"],
            fontWeight: 100,
        }
    };
    const optionKgBar = {
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        }, grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0.5,
            },
        },
        yaxis: {
            max: yAxisMaxValue,
        },
        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },
        series: [
            {
                name: '',
                data: listQuantities.map((item) => item.quantity)
            }
        ],

        labels: listQuantities.map((data) => {
            const formattedDate = dayjs(data.date).format("DD/MM");
            return formattedDate;
        }),
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B', "#429D44"],
        tooltip: {
            enabled: true,
            theme: "dark",

        },

        legend: {
            show: false,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 400,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },
        dataLabels: dataLabelsOptions,
    };

    const maxDataValueBag = listQuantityAo.reduce((max, data) => Math.max(max, data["500"], data["450"]), 0);
    const yAxisMaxValueBag = Math.max(maxDataValueBag, maxDataValueBag + 3);
    const optionsBagBar = {
        chart: {
            type: 'bar',
            zoom: {
                type: "x",
                enabled: true,
            },
        }, grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0.5,
            },
        },
        yaxis: {
            max: yAxisMaxValueBag,
        },
        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 2,
                dataLabels: {
                    position: "top",
                },
            },
        },

        labels: listQuantityAo.map((data) => {
            const formattedDate = dayjs(data.date).format("DD/MM");
            return formattedDate;
        }),
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 1,
            dashArray: [0, 0, 8]
        },
        colors: ['#036E9B', "#429D44"],
        tooltip: {
            enabled: true,
            theme: "dark",

        },

        legend: {
            show: true,
            showForSingleSeries: true,
            showForNullSeries: true,
            showForZeroSeries: false,
            position: "top",
            horizontalAlign: "center",
            floating: true,
            fontSize: "13px",
            fontFamily: "Helvetica, Arial",
            fontWeight: 400,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            customLegendItems: [],
            offsetX: 0,
            offsetY: 0,
        },

        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val;
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#000"],
                fontWeight: 100,

            }
        },
    };
    const columnKg = [
        {
            title: <p className='nuni-font ' >{t('reportTable.areaName')}</p>,
            dataIndex: 'fishPond',
            render: (fishPond) => <p style={{ fontWeight: 400 }} className='nuni-font  ' >{fishPond.farmingArea.name}</p>,
        },
        {
            title: <p className='nuni-font '   >{t('reportTable.pondName')}</p>,
            dataIndex: 'fishPond',
            render: (fishPond) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{fishPond.name}</p>,
        },
        {
            title: <p className='nuni-font '>{t('reportTable.dayExport')}</p>,
            dataIndex: 'timeStamp',
            render: (timeStamp) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{timeStamp ? dayjs(timeStamp).format('DD-MM-YYYY') : ''}</p>,
        },
        {
            title: <p className='nuni-font '>{t('reportTable.timeExport')}</p>,
            dataIndex: 'dateOfManufacture',
            render: (dateOfManufacture) => (
                <p style={{ fontWeight: 400 }} className='nuni-font '>
                    {dateOfManufacture ? dayjs(dateOfManufacture).format('DD-MM-YYYY') : ''}
                </p>
            )
        },
        {
            title: <p className='nuni-font '>{t('reportTable.quantity')}</p>,
            dataIndex: 'quantity',
            render: (quantity) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{quantity} Kg</p>,
        },
        {
            title: <p className='nuni-font  ' >{t('reportTable.ordinalNum')}</p>,
            dataIndex: 'ordinalNum',
            render: (ordinalNum) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{ordinalNum} </p>,
        },
        {
            title: <p className='nuni-font ' >{t('reportTable.lotNum')}</p>,
            dataIndex: 'lotNum',
            render: (lotNum) => <p style={{ fontWeight: 400 }} className='nuni-font ' >{lotNum} </p>,
        },
        {
            title: <p className=' nuni-font ' >{t('reportTable.qrCode')}</p>,
            dataIndex: 'barCode',
            render: (barCode) => <p style={{ fontWeight: 400 }} className=' ' >{barCode} </p>,
        },
        {
            title: <p className='nuni-font ' >{t('reportTable.status')}</p>,
            fixed: 'right',
            dataIndex: 'flag',
            render: (flag) => {
                if (flag === -4) {
                    return <p className='text-center nuni-font ' style={{ padding: 3, borderRadius: 5, fontSize: 13, background: '#EF4444', color: 'white', }}>{t('reportTable.error')}</p>
                } else {
                    return <p className='text-center nuni-font' style={{ padding: 3, borderRadius: 5, fontSize: 13, background: '#22C55E', color: 'white', }}>{t('reportTable.success')}</p>
                }
            }
        },
    ];

    const handleSaveConfirmation = () => {
        Modal.confirm({
            title: <span>{t('edit.confirm')}</span>,
            content: <span>{t('edit.confirmSave')}</span>,
            okText: <span style={{ color: 'white' }}>{t('edit.cancel')}</span>,
            cancelText: <span>{t('edit.save')}</span>,
            onOk: handleModalOk,
            centered: true,
            maskClosable: true,
            width: '300px'
        });
    };
    const disabledDateTime = () => ({
        disabledHours: () => range(0, 24).splice(4, 20),
        disabledMinutes: () => range(30, 60),
        disabledSeconds: () => [55, 56],
    });
    const range = (start, end) => {
        const result = [];
        for (let i = start; i < end; i++) {
            result.push(i);
        }
        return result;
    };

    const handleBarCodeChange = (event) => {
        const { value } = event.target;
        const barCodeParts = value.split(' ');
        const lotNum = barCodeParts[1];
        const quantity = barCodeParts[2];
        const quantityMap = {
            '1': 40,
            '2': 500,
            '3': 450,
        };
        const updatedQuantity = quantityMap[quantity] || quantity;

        setFormValues((prevValues) => ({
            ...prevValues,
            barCode: value,
            lotNum,
            quantity: updatedQuantity,
            ordinalNum: barCodeParts[3],
        }));
    };


    const handleRowClick = (record) => {
        setSelectedRow(selectedRow === record ? null : record)
        handleSecondaryButtonClick()
        setFormValues(record);
        setShowForm(true);

    };

    const handleCreate = async (data) => {
        try {
            const response = await https.post(ApiUpdateFeed, { data });
            message.success('Thay đổi thành công');
            const index = nameTableKg.findIndex((item) => item.id === data.id);
            if (index !== -1) {
                nameTableKg[index] = data;
                setNameTableKg(nameTableKg);
                loadChart()
            }
        } catch (error) {
            console.log('error: ', error);
        }
    };



    const handleFormSubmit = (event) => {
        event.preventDefault();
        const updatedFormValues = { ...formValues };
        handleCreate(updatedFormValues);
    };

    const handleFormChange = (event) => {
        const { name, value } = event.target;
        setFormValues((prevValues) => ({ ...prevValues, [name]: value }));
    };

    const handleModalCancel = (record) => {
        setIsModalVisible(false);
        setFormValues({});
        selectedRow === record ? setSelectedRow(record) : setSelectedRow(record)
    };

    const handleModalOk = () => {
        setIsModalVisible(false);
        handleCreate(formValues);

    };

    const handleSecondaryButtonClick = () => {
        setIsModalVisible(true);
    };
    const customLocale = {
        lang: {
            locale: 'vi',
            placeholder: 'Chọn ngày',
            rangePlaceholder: [
                [t('time.start')],
                [t('time.end')]
            ],
            yearFormat: '/ YYYY',
            dateFormat: 'DD/MM/YYYY',
            dayFormat: 'D',
            dateTimeFormat: 'DD/MM/YYYY HH:mm:ss',
            monthBeforeYear: true,
            shortWeekDays: [[t('week.2')], [t('week.3')], [t('week.4')], [t('week.5')], [t('week.6')], [t('week.7')], [t('week.cn')]],
            shortMonths: [[t('month.1')], [[t('month.2')]], [[t('month.3')]], [[t('month.4')]], [[t('month.5')]], [[t('month.6')]], [[t('month.7')]], [[t('month.8')]], [[t('month.9')]], [[t('month.10')]], , [[t('month.11')]], [[t('month.12')]],],
        },
    };
    useEffect(() => {
        // Lấy id của phần tử đầu tiên trong danh sách ao sau khi filteredData được cập nhật
        if (filteredData && filteredData.length > 0) {
            setSelectedPondId(filteredData[0].id);
            setAoId(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu
        }
    }, [filteredData]);

    const renderSelectPage = () => {
        return <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
            <div className=' flex  ' style={{ alignItems: 'center', }}>
                <AiOutlineHome size={20} />
                <a className='px-2  ' style={{ fontSize: 16 }} >Home  </a>
                <p>/</p>
                <Select
                    bordered={false}
                    value={selectedFarmingAreaId}
                    onChange={handleSelectChange}

                    placeholder={<span className='text-black'>{t('select.farmingArea')}</span>}
                >
                    {loadNameFaming.map((item) => (
                        <Select key={item.id} value={item.id}>
                            <div className='flex  ' style={{ color: '#', }}>
                                {item.name}
                            </div>
                        </Select>
                    ))}
                </Select>
                <p >/</p>
                <Select
                    bordered={false}
                    placeholder={<span className='text-black'>{t('select.pond')}</span>}
                    value={selectedPondId}

                    onChange={changeValueCallApiNumber3}
                >
                    {filteredData.map((item) => (
                        <Select.Option key={item.id} value={item.id}>
                            <div className='flex  ' style={{ color: '#', }}>
                                {item.name}
                            </div>
                        </Select.Option>
                    ))}
                </Select>
            </div>
            <div className='date-picker-container' >
                <DatePicker
                    onChange={handleStartDateChange}
                    defaultValue={dayjs(startDateKgDefault)}
                    format={dateFormat}

                    locale={customLocale}
                />
                <DatePicker
                    onChange={handleEndDateChange}
                    defaultValue={dayjs()}
                    format={dateFormat}
                    locale={customLocale}

                />
            </div>
        </div >
    }

    const resPc = () => {
        return (
            <div className='' >
                <div className='flex container-chart  ' style={{ padding: 10, gap: 5 }} >
                    <div className='chartKg' >
                        <div className=" text-titleBorder flex " style={{ color: " white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('report.titleKg')}
                            </p>
                            <div className='button-add-feed' >
                                <ChartCreate />
                            </div>
                            <div className='button-add-feed1'>
                                <AddNewListMobile />
                            </div>
                        </div>
                        <div className='bg-white borderChartBottom ' >
                            <ReactApexChart
                                className="height_chart_mobile"
                                options={optionKgBar}
                                series={optionKgBar.series}
                                type="bar"
                                height={350}
                            />
                        </div>
                    </div >
                    <div className='chartKg' >
                        <div className=" text-titleBorder flex " style={{ color: " white", fontWeight: 400, background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }} >{t('report.titleBag')}
                            </p>
                        </div>
                        <div className='bg-white borderChartBottom ' >
                            <ReactApexChart
                                className="height_chart_mobile"
                                options={optionsBagBar}
                                series={[
                                    { name: "450 Kg", data: listQuantityAo.map((item) => item["450"]), strokeWidth: 1 },
                                    { name: "500 Kg", data: listQuantityAo.map((item) => item["500"]), strokeWidth: 1 },
                                ]}
                                type="bar"
                                height={350}
                            />
                        </div>
                    </div>
                </div>
                <div className='' style={{ padding: '0px 10px' }} >
                    <div className=" text-titleBorder flex " style={{ color: "white", fontWeight: 400, background: "#036E9B", }} >
                        <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>{t('report.titleTable')}
                        </p>
                    </div>
                    <div style={{ overflowX: 'auto' }} >
                        <Table
                            scroll={{ x: 'max-content', }}
                            onRow={(record) => ({
                                onClick: () => handleRowClick(record),
                                className: record === selectedRow ? 'table-row-selected  min-w-full' : '',
                            })} dataSource={nameTableKg} columns={columnKg} pagination={{
                                pageSize: 10,
                            }} />
                    </div>

                </div>
            </div>
        )
    }

    const modalCustoms = () => {
        return <div>
            <Modal
                title={
                    <span >
                        <div className="flex py-5">
                            {' '}
                            <AiOutlineInfoCircle size={25} style={{ borderRadius: 50, background: 'orange', color: 'white' }} />{' '}
                            <p className="px-2">{t('edit.title')} </p>
                        </div>
                        <div className='flex py-2' >
                            <Input
                                style={{ width: 270 }}
                                prefix={<FcHome size={24} />}

                                name="fishPond"
                                value={formValues.fishPond ? formValues.fishPond.farmingArea.name : ''}
                            />
                            <Input
                                style={{ width: 270, marginLeft: 10 }}

                                prefix={<FaWater size={24} />}

                                name="fishPond"
                                value={formValues.fishPond ? formValues.fishPond.name : ''}
                            />
                        </div>
                    </span>
                }
                visible={isModalVisible}
                onOk={handleSaveConfirmation}
                onCancel={handleModalCancel}
                okText={<span style={{ border: 1 }} >{t('edit.save')}</span>}
                cancelText={<span >{t('edit.cancel')} </span>}
                width={600}
            >
                {showForm && (
                    <div >
                        <Form onSubmit={handleFormSubmit}>
                            <div className='flex editModalMobile ' style={{ gap: 10 }} >
                                <div className='' >
                                    <Form.Item>
                                        <p className='text-blue-600' > {t('edit.feedingday')}</p>

                                        <DatePicker
                                            style={{ width: 270, height: 50 }}

                                            format="DD-MM-YYYY HH:mm:ss"
                                            value={formValues.timeStamp ? dayjs(formValues.timeStamp) : null}
                                            disabledTime={disabledDateTime}
                                            showTime={{
                                                defaultValue: dayjs('00:00:00', 'HH:mm:ss'),
                                            }}
                                            onChange={(date, dateString) => {
                                                setFormValues({ ...formValues, timeStamp: dateString });
                                            }}
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <p className='text-blue-600'>Bar code</p>
                                        <Input
                                            style={{ width: 270, height: 50 }}
                                            prefix={<BsQrCode size={20} />}
                                            name="barCode"
                                            value={formValues.barCode || ''}
                                            onChange={handleBarCodeChange}
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <p className='text-blue-600'>{t('edit.odernum')}</p>
                                        <Input
                                            prefix={<AiOutlineOrderedList size={24} />}
                                            style={{ width: 270, height: 50 }}
                                            type='number'
                                            name="ordinalNum"
                                            value={formValues.ordinalNum || ''}
                                            onChange={handleFormChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div  >
                                    <Form.Item>
                                        <p className='text-blue-600' >{t('edit.production')}</p>
                                        <DatePicker
                                            style={{ width: 270, height: 50 }}
                                            format="YYYY-MM-DD HH:mm:ss"
                                            value={formValues.dateOfManufacture ? dayjs(formValues.dateOfManufacture) : null}
                                            disabledTime={disabledDateTime}
                                            showTime={{
                                                defaultValue: dayjs('00:00:00', 'HH:mm:ss'),
                                            }}
                                            onChange={(date, dateString) => {
                                                setFormValues({ ...formValues, dateOfManufacture: dateString });
                                            }}
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <p className='text-blue-600'>{t('edit.quantity')}</p>
                                        <Input
                                            prefix={<MdScale size={24} />}

                                            style={{ width: 270, height: 50 }}
                                            type='number'

                                            name="quantity"
                                            value={formValues.quantity || ''}
                                            onChange={handleFormChange}
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <p className='text-blue-600'>{t('edit.lotnum')}</p>
                                        <Input
                                            prefix={<TbNumber size={24} />}
                                            style={{ width: 270, height: 50 }}
                                            type='number'
                                            name="lotNum"
                                            value={formValues.lotNum || ''}
                                            onChange={handleFormChange}
                                        />
                                    </Form.Item>
                                </div>
                            </div>
                        </Form>
                        <p style={{ color: 'red' }}>{t('edit.warning')}</p>
                    </div>
                )}
            </Modal>
        </div>
    }

    return (
        <div className='nuni-font' >
            {renderSelectPage()}
            {resPc()}
            {/* {resMobile()} */}

            {modalCustoms()}

        </div >

    )
}


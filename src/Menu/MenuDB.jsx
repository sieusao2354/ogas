import React, { useEffect, useState } from 'react';
import { Layout, Menu } from 'antd';
import { AiOutlineBars } from 'react-icons/ai';

import { FaBars } from 'react-icons/fa';

import ReportFood from '../Report/ReportFood';
import Testcode from '../ChartAccumulated/Testcode';
import Monitor from '../Monitor/Monitor';

const { Sider, Content } = Layout;

const SubMenu = ({ title, icon, items, onClick }) => {
    return (
        <Menu.SubMenu key={title} icon={icon} title={title}>
            {items.map((item) =>
                item.children ? (
                    <SubMenu
                        key={item.key}
                        title={item.label}
                        icon={item.icon}
                        items={item.children}
                        onClick={onClick}
                    />
                ) : (
                    <Menu.Item key={item.key} icon={item.icon} onClick={onClick}>
                        {item.label}
                    </Menu.Item>
                )
            )}
        </Menu.SubMenu>
    );
};

const MenuDB = () => {


    const [selectedComponent, setSelectedComponent] = useState('component1');
    const [collapsed, setCollapsed] = useState(false); // State để lưu trạng thái của menu
    const [buttonExpanded, setButtonExpanded] = useState(false);
    const handleMenuClick = (e) => {
        setSelectedComponent(e.key);
        localStorage.setItem('report', e.key);
    };
    const handleCollapse = () => {
        setCollapsed(!collapsed); // Thay đổi trạng thái collapsed khi nhấn nút
        setButtonExpanded((prevExpanded) => !prevExpanded);
    };
    const storedComponent = localStorage.getItem('report');

    useEffect(() => {
        if (storedComponent) {
            setSelectedComponent(storedComponent);
        }
    }, [storedComponent]);


    const renderComponent = () => {
        if (!selectedComponent) {
            // Set the default component when there is no selected component in local storage
            setSelectedComponent('report');
        }

        switch (selectedComponent) {
            case 'report':
                return <ReportFood />
            case 'Monitor':
                return <Monitor />
            case 'Test':
                return <Testcode />

            default:
                return null;
        }
    };

    const menuItems = [

        {
            icon: <AiOutlineBars size={16} />,
            key: 'report', label: 'Báo cáo ',
        }

    ]

    const menuItems2 = [


        {
            icon: <AiOutlineBars size={16} />,
            key: 'Monitor', label: 'Giám sát',
        }
    ]

    const menuItems3 = [
        {
            icon: <AiOutlineBars size={16} />,
            key: 'Test', label: 'Điều khiển',
        }
    ]

    return (
        <div >
            <Layout style={{}}  >
                <Sider className='py-1' style={{ background: '#fff', }} trigger={null} collapsed={collapsed} onCollapse={handleCollapse}>
                    <button
                        onClick={() => setCollapsed(!collapsed)}
                        type="text"
                        style={{
                            fontSize: '30px',
                            borderRadius: 5,
                        }}
                    >
                        <FaBars />
                    </button>
                    <Menu style={{ textAlign: 'left', fontSize: 13 }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuClick}>
                        {menuItems.map((item) =>
                            item.children ? (
                                <SubMenu
                                    key={item.key}
                                    title={item.label}
                                    icon={item.icon}
                                    items={item.children}
                                    onClick={handleMenuClick}
                                />
                            ) : (
                                <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuClick}>
                                    {item.label}
                                </Menu.Item>
                            )
                        )}
                    </Menu>
                    <hr />

                    <Menu style={{ textAlign: 'left', fontSize: 13 }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuClick}>
                        {menuItems2.map((item) =>
                            item.children ? (
                                <SubMenu

                                    key={item.key}
                                    title={item.label}
                                    icon={item.icon}
                                    items={item.children}
                                    onClick={handleMenuClick}
                                />
                            ) : (
                                <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuClick}>
                                    {item.label}

                                </Menu.Item>
                            )
                        )}
                    </Menu>
                    <hr />

                    <Menu style={{ textAlign: 'left', fontSize: 13 }} mode="inline" selectedKeys={[selectedComponent]} defaultOpenKeys={menuItems.map(item => item.key)} onClick={handleMenuClick}>
                        {menuItems3.map((item) =>
                            item.children ? (
                                <SubMenu

                                    key={item.key}
                                    title={item.label}
                                    icon={item.icon}
                                    items={item.children}
                                    onClick={handleMenuClick}
                                />
                            ) : (
                                <Menu.Item key={item.key} icon={item.icon} onClick={handleMenuClick}>
                                    {item.label}

                                </Menu.Item>
                            )
                        )}
                    </Menu>


                    <div style={{ height: 100 }} ></div>
                </Sider>
                <Layout  >
                    <Content style={{}} >{renderComponent()}</Content>

                </Layout>
                <div style={{ height: 100 }} ></div>

            </Layout >
        </div >
    );
};


export default MenuDB
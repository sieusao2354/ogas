import React, { useEffect, useState } from 'react';
import { ApiQuanTrac, https, listFarmingArea, loadFishPond, loadFishPondData, startAndlimit } from '../Service/ConFigURL';
import ReactApexChart from 'react-apexcharts';
import { DatePicker, Input, Modal, Select, Table } from 'antd';
import dayjs from 'dayjs';
import { AiFillHome, AiOutlineDownCircle, AiOutlineDownload, AiOutlineHome } from 'react-icons/ai';
import { t } from 'i18next';
import "./QuanTrac.css"
import * as XLSX from 'xlsx';

export default function QuanTrac() {
    const dateFormat = 'YYYY-MM-DD';
    const [VungNuoi, setVungNuoi] = useState([])
    const [selectAoId, setSelectAoId] = useState()

    const [parameter, setParameter] = useState([]);
    const [nameValue, setNameValue] = useState([])
    const [filteredData, setFilteredData] = useState([]);

    const [selectedThongsoid, setSelectedThongsoid] = useState(1)
    const [aoId, setAoId] = useState([])
    const [selectPond, setSelectPond] = useState([])
    const [startDate, setStartDate] = useState(dayjs().format(dateFormat)); // Đặt ngày bắt đầu là ngày hiện tại
    const [endDate, setEndDate] = useState(dayjs().format(dateFormat)); // Đặt ngày kết thúc là ngày hiện tại
    const [selectedPondId, setSelectedPondId] = useState(null);
    const [customFileName, setCustomFileName] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [Value, setValue] = useState([])

    const { Option } = Select;

    const vungNuoi = async () => {
        try {
            const token = localStorage.getItem('token');
            const res = await https.post(listFarmingArea, startAndlimit)
            if (token) {
                setVungNuoi(res.data.data.list)
                if (res.data.data.list && res.data.data.list.length > 0) {
                    setSelectAoId(res.data.data.list[0].id);
                }
            }

        } catch (err) {
            console.log('err: ', err);
        }
    }

    const pondId = async () => {
        try {
            const token = localStorage.getItem('token')
            const res = await https.post(loadFishPond, loadFishPondData)
            if (token) {
                setAoId(res.data.data.list)

            }
        } catch (err) {
            console.log('err: ', err);
        }
    }

    useEffect(() => {
        vungNuoi()
        pondId()
    }, [])

    useEffect(() => {
        if (selectAoId) {
            const filteredPonds = aoId.filter(
                (item) =>
                    item.farmingArea && item.farmingArea.id === selectAoId
            );
            setFilteredData(filteredPonds);
        } else {
            setFilteredData(aoId);
        }
    }, [selectAoId, aoId]);

    const changeValueCallApiNumber3 = (value) => {
        setSelectPond(value)
        setSelectedPondId(value);

    };

    const handleSelectChange = (value) => {
        setSelectAoId(value);
    };


    useEffect(() => {
        fetchMonitoring()


    }, [selectedThongsoid, selectPond, endDate, startDate,])

    const handleStartDateChange = (date, dateString) => {

        setStartDate(dateString);

    };

    const handleEndDateChange = (date, dateString) => {

        setEndDate(dateString);

    };

    const fetchMonitoring = async () => {
        if (!selectPond || !selectedThongsoid || !startDate || !endDate) return
        const params = {
            "data": {
                "starttime": startDate,
                "endtime": endDate,
                "aoid": selectPond,
                "thongsoid": selectedThongsoid
            }
        }

        try {
            const token = localStorage.getItem('token');
            const res = await https.post(ApiQuanTrac, params);

            if (token) {

                setValue(res.data.data.datatable)
                const lstDate = res.data.data.datachart.lstDate || []

                if (lstDate.length > 0) {
                    setParameter(lstDate)
                    setNameValue(res.data.data.datatable[0].parameter)
                } else {
                    setParameter([]);
                    setNameValue([]);
                }

            }

        } catch (error) {
            console.log('error: ', error);
        }
    }

    const handleThongsoidChange = (value) => {

        setSelectedThongsoid(parseInt(value));
    }

    const options = {

        chart: {
            type: 'line-chart',
            zoom: {
                type: "x",
                enabled: true,
            },
            height: "120"
        },

        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // Màu của các dòng lưới (gạch hàng dọc) và dòng tiếp theo
                opacity: 1, // Độ mờ của các dòng lưới
            },
        },

        noData: {
            text: "No data",
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
                color: undefined,
                fontSize: '14px',
                fontFamily: undefined
            }
        },

        plotOptions: {
            bar: {
                borderRadius: 1.5,
                dataLabels: {
                    dataLabels: {
                        enabled: false,
                        position: "top",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold",
                            colors: [""],
                        },
                    },
                },
            },
        },

        xaxis: {
            categories: parameter.map((data) => {
                const dateParts = data.date.slice(5, 16)
                return dateParts
            }),
        },

        legend: {
            position: 'top',
        },

        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: undefined,
            width: 2,
            dashArray: [4, 0, 4]
        },

        colors: ['#EF4444', '#0E9CFF', '#EF4444'],

        tooltip: {
            enabled: true,
            theme: "dark",
            x: {
                show: false,
                format: "yyyy-MM-dd",

            },
        },

        yaxis: {
            show: true,
            min: selectedThongsoid === 1 ? 0 : selectedThongsoid === 2 ? -2 : selectedThongsoid === 3 ? 0 : undefined,
            max: selectedThongsoid === 1 ? 14 : selectedThongsoid === 2 ? 14 : selectedThongsoid === 3 ? 40 : undefined,
        },

        series: [
            {
                name: 'UCL',
                data: parameter.map((item) => item.ucl),
                strokeWidth: 1,
            },
            {
                name: nameValue.name,
                data: parameter.map((item) => item.value),
                strokeWidth: 1,
            },
            {
                name: 'LCL',
                data: parameter.map((item) => item.lcl),
                strokeWidth: 1,
            }
        ],
    };



    const columns = [
        {
            title: <span>{t('monitoringTable.areaName')}</span>,
            dataIndex: "fishPond",
            key: "fishPond",
            render: (fishPond) => <span>{fishPond.farmingArea.name}</span>
        },
        {
            title: <span>{t('monitoringTable.pondName')}</span>,

            dataIndex: "fishPond",
            key: "fishPond",
            render: (fishPond) => <span>{fishPond.name}</span>
        },
        {
            title: <span>{t('monitoringTable.dayCheck')}</span>,
            dataIndex: "timeStamp",
            key: "timeStamp",
            render: (timeStamp) => <span>{timeStamp.slice(0, 11)}</span>
        },
        {
            title: <span>{t('monitoringTable.timeCheck')}</span>,
            dataIndex: "timeStamp",
            key: "timeStamp",
            render: (timeStamp) => <span>{timeStamp.slice(11, 16)}</span>
        },
        {
            title: "UCL",
            dataIndex: "ucl",
            key: "ucl",
        },
        {
            title: nameValue.name,
            dataIndex: "value",
            key: "value",
        },
        {
            title: "LCL",
            dataIndex: "lcl",
            key: "lcl",
        },
        {
            title: <span>{t('monitoringTable.status')}</span>,
            fixed: 'right',

            dataIndex: "evaluate",
            key: "evaluate",
            render: (text) => {
                if (text === "FAIL") {
                    return <p className='bg-red-500 text-white  text-center' style={{ borderRadius: 3 }} >{t('monitoringTable.unsatisfactory')}</p>
                } else if (text === "PASS") {
                    return <p className='bg-green-500 text-center  text-white' style={{ borderRadius: 3 }} > {t('monitoringTable.qualified')}</p>
                }
            }
        },
    ];

    useEffect(() => {
        // Lấy id của phần tử đầu tiên trong danh sách ao sau khi filteredData được cập nhật
        if (filteredData && filteredData.length > 0) {
            setSelectedPondId(filteredData[0].id);
            setSelectPond(filteredData[0].id); // Gọi loadChart với id của ao đầu tiên khi render lần đầu
        }
    }, [filteredData]);
    const exportToExcel = () => {
        const workbook = XLSX.utils.book_new();
        const data = parameter.map((data, index) => ({
            Date: parameter[index].timeStamp.slice(0, 10),
            times: parameter[index].timeStamp.slice(11, 16),
            [nameValue.code]: options.series[1].data[index],
        }));
        const worksheet = XLSX.utils.json_to_sheet(data);

        const fileName = customFileName || '';
        XLSX.utils.book_append_sheet(workbook, worksheet, fileName);
        XLSX.writeFile(workbook, `${fileName}.xlsx`);
    };

    const showExportModal = () => {
        setIsModalVisible(true);
    };
    const handleExport = () => {
        setCustomFileName(customFileName);
        exportToExcel();
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <div>
            <div className=' bg-white responsive_select ' style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', }}>
                <div className=' flex  ' style={{ alignItems: 'center', }}>
                    <AiOutlineHome size={20} />
                    <a className='px-2  ' style={{ fontSize: 14 }} >Home  </a>
                    <p>/</p>

                    <Select
                        bordered={false}
                        value={selectAoId}
                        onChange={handleSelectChange}
                        defaultValue={"Chọn vùng nuôi"}
                    >
                        {VungNuoi.map((item) => (
                            <Select key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 14, color: '#' }}>
                                    {item.name}
                                </div>
                            </Select>
                        ))}
                    </Select>
                    -
                    <Select
                        bordered={false}
                        placeholder={<span className='text-black'>{t('select.pond')}</span>}
                        value={selectedPondId}

                        onChange={changeValueCallApiNumber3}>

                        {filteredData.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                <div className='flex ' style={{ fontWeight: 400, fontSize: 14, color: '#' }}>
                                    {item.name}
                                </div>
                            </Select.Option>
                        ))}
                    </Select>
                    <Select style={{ width: 150 }} value={selectedThongsoid} onChange={handleThongsoidChange}>
                        <Option value={1}>pH</Option>
                        <Option value={2}>DO</Option>
                        <Option value={3}>Nhiệt độ</Option>
                    </Select>
                    <button className='bg-white  mx-2 px-2 ' style={{ borderRadius: 5 }} onClick={showExportModal}><AiOutlineDownCircle size={30} /></button>

                </div>
                <div className=' ' >

                    <DatePicker
                        onChange={handleStartDateChange}
                        defaultValue={dayjs()}
                        format={dateFormat}

                    />

                    <DatePicker
                        onChange={handleEndDateChange}
                        defaultValue={dayjs()}
                        format={dateFormat}

                    />
                </div>
            </div>

            <div className="  py-3 "  >
                <div className='flex ' >
                    <div style={{ width: 10 }} ></div>

                    <div className='bg-white ' style={{ borderRadius: 5, width: '100%' }}>
                        <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                            <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>{t('monitoring.title')}</p>
                        </div>
                    </div>
                    <div style={{ width: 10 }} ></div>
                </div>
                <div className='flex' >
                    <div style={{ width: 10 }}  >
                    </div>
                    <div className='bg-white monitoringPC' style={{ width: '100%', borderRadius: 5 }} >
                        <ReactApexChart
                            options={options}
                            series={options.series}
                            type="line"
                            height={450}

                        />
                    </div>
                    <div className='bg-white monitoringMB' style={{ width: '100%', borderRadius: 5 }} >
                        <ReactApexChart
                            options={options}
                            series={options.series}
                            type="line"
                            height={300}

                        />
                    </div>
                    <div style={{ width: 10 }}  >
                    </div>
                </div>
                <div className='py-3' >
                    <div className='flex ' >
                        <div style={{ width: 10 }} ></div>

                        <div className='bg-white ' style={{ borderRadius: 5, width: "100%" }}>
                            <div className=" text-titleBorder flex " style={{ color: "white", background: "#036E9B", }} >
                                <p style={{ padding: 7, marginLeft: 5, fontWeight: 500, fontSize: 15, letterSpacing: 0.5 }}>{t('monitoring.titleTable')}
                                </p>
                            </div>
                        </div>
                        <div style={{ width: 10 }} ></div>
                    </div>
                    <div className='flex' >

                        <div style={{ width: 10 }}  >
                        </div>
                        <div style={{ width: '100%' }}  >
                            <Table
                                scroll={{ x: 'max-content', }}

                                dataSource={Value} pagination={{ pageSize: 10 }} columns={columns} />
                        </div>
                        <div style={{ width: 10 }}  >
                        </div>
                    </div>
                </div>
            </div>

            <Modal
                title="Nhập tên tệp Excel"
                visible={isModalVisible}
                onOk={handleExport}
                onCancel={handleCancel}
            >
                <Input
                    type="text"
                    placeholder="Nhập tên tệp Excel"
                    onChange={(e) => setCustomFileName(e.target.value)}
                    value={customFileName}
                />
            </Modal>

        </div>
    );
}

//